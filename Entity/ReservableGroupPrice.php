<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Entity;

use Comsa\SuluReservations\Entity\Interfaces\CrudResource;
use Comsa\SuluReservations\Utility\TypeConverter;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use JetBrains\PhpStorm\ArrayShape;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

#[
    Entity(),
    Table(name: "comsa_re_reservable_group_prices"),
    ExclusionPolicy()
]
class ReservableGroupPrice
{
    #[
        Id(),
        GeneratedValue(strategy: "AUTO"),
        Column(type: Types::INTEGER),
        Expose()
    ]
    private ?int $id;

    #[
        ManyToOne(targetEntity: "Comsa\SuluReservations\Entity\Reservable", inversedBy: "groupPrices"),
        JoinColumn(name: "reservable_id", referencedColumnName: "id"),
        Expose()
    ]
    private ?Reservable $reservable;

    #[
        ManyToOne(targetEntity: "Comsa\SuluReservations\Entity\GroupPrice"),
        JoinColumn(name: "group_price_id", referencedColumnName: "id"),
        Expose()
    ]
    private ?GroupPrice $groupPrice;

    #[
        Column(type: Types::INTEGER),
        Expose()
    ]
    private ?int $position;

    public function getId(): ?int {
        return $this->id;
    }

    public function getReservable(): ?Reservable {
        return $this->reservable;
    }

    public function setReservable(?Reservable $reservable): self {
        $this->reservable = $reservable;

        return $this;
    }

    public function getGroupPrice(): ?GroupPrice {
        return $this->groupPrice;
    }

    public function setGroupPrice(GroupPrice $groupPrice): self {
        $this->groupPrice = $groupPrice;

        return $this;
    }

    public function getPosition(): ?int {
        return $this->position;
    }

    public function setPosition(int $position): self {
        $this->position = $position;

        return $this;
    }

    public function __toString(): string {
        return sprintf("%s%d", $this->getGroupPrice()->getGroupName(), $this->getGroupPrice()->getPrice());
    }

    #[ArrayShape([
        "id" => "int",
        "type" => "string",
        "groupName" => "string",
        "groupPrice" => "float",
        "groupPosition" => "int",
        "amount" => "int"
    ])]
    public function toArray(): array
    {
        return [
            "id" => $this->getGroupPrice()->getId(),
            "type" => "price",
            "groupName" => $this->getGroupPrice()->getGroupName(),
            "groupPrice" => TypeConverter::floatToString($this->getGroupPrice()->getPrice()),
            "groupPosition" => $this->getPosition(),
            "amount" => 0
        ];
    }
}
