<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Entity;

use Comsa\SuluReservations\Entity\Interfaces\CrudResource;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

#[
    Entity(),
    Table(name: "comsa_re_tax_categories"),
    ExclusionPolicy("all")
]
class TaxCategory implements CrudResource
{
    const RESOURCE_KEY = "comsa_re_tax_categories";

    #[
        Id(),
        GeneratedValue(strategy: "AUTO"),
        Column(type: Types::INTEGER),
        Expose()
    ]
    private ?int $id;

    #[
        Column(type: Types::STRING, length: 255),
        Expose()
    ]
    private ?string $title;

    #[
        Column(type: Types::DECIMAL, precision: 5, scale: 2),
        Expose()
    ]
    private ?float $percentage;

    public function getId(): ?int {
        return $this->id;
    }

    public function getTitle(): ?string {
        return $this->title;
    }

    public function setTitle(string $title): self {
        $this->title = $title;

        return $this;
    }

    public function getPercentage(): ?float {
        return $this->percentage;
    }

    public function setPercentage(float $percentage): self {
        $this->percentage = $percentage;

        return $this;
    }

    public function toString(): string {
        return sprintf(
            "(%d%%)",
            $this->getPercentage() * 100
        );
    }
}
