<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Entity;

use Comsa\SuluReservations\Entity\Interfaces\CrudResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OrderBy;
use Doctrine\ORM\Mapping\Table;
use JetBrains\PhpStorm\ArrayShape;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\VirtualProperty;
use Sulu\Bundle\FormBundle\Entity\Form;
use Comsa\SuluReservations\Entity\TaxCategory;

#[
    Entity(),
    Table(name: "comsa_re_reservables"),
    ExclusionPolicy("all"),
    VirtualProperty(name: "groupPrices", exp: "object.getGroupPricesForBlocks()")
]
class Reservable implements CrudResource
{
    const RESOURCE_KEY = "comsa_re_reservables";

    #[
        Id(),
        GeneratedValue(strategy: "AUTO"),
        Column(type: Types::INTEGER),
        Expose()
    ]
    private ?int $id;

    #[
        ManyToOne(targetEntity: "Sulu\Bundle\FormBundle\Entity\Form"),
        JoinColumn(name: "form_id", referencedColumnName: "id"),
        Expose()
    ]
    private ?Form $form;

    #[
        Column(type: Types::STRING, length: 255),
        Expose()
    ]
    private ?string $title;

    #[
        Column(type: Types::BOOLEAN),
        Expose()
    ]
    private bool $singleReservation;

    #[
        Column(name: "`limit`", type: Types::INTEGER),
        Expose()
    ]
    private ?int $limit;

    #[
        Column(type: Types::STRING, length: 255),
        Expose()
    ]
    private ?string $type;

    #[
        Column(type: Types::STRING, length: 255),
        Expose()
    ]
    private ?string $priceType;

    #[
        Column(type: Types::DECIMAL, nullable: true, precision: 10, scale: 2),
        Expose()
    ]
    private ?float $singlePrice;

    #[
        Column(type: Types::DATETIME_MUTABLE),
        Expose()
    ]
    private ?\DateTime $end;

    #[
        Column(type: Types::DATETIME_MUTABLE),
        Expose()
    ]
    private ?\DateTime $start;

    #[
        Column(type: Types::STRING, length: 255, nullable: true),
        Expose()
    ]
    private ?string $pageId = null;

    #[
        Column(type: Types::STRING, length: 255),
        Expose()
    ]
    private ?string $parentPage;

    #[
        OneToMany(targetEntity: "Comsa\SuluReservations\Entity\ReservableGroupPrice", mappedBy: "reservable", cascade: [ "persist", "remove" ]),
        OrderBy([ "position" => "ASC" ])
    ]
    private Collection $groupPrices;

    #[
        Column(type: Types::STRING, length: 255, nullable: true),
        Expose()
    ]
    private ?string $optionSelectionType;

    #[
        ManyToOne(targetEntity: "Comsa\SuluReservations\Entity\TaxCategory"),
        JoinColumn(name: "tax_category_id", referencedColumnName: "id")
    ]
    private ?TaxCategory $taxCategory;

    #[
        OneToMany(targetEntity: "Comsa\SuluReservations\Entity\ReservableOption", mappedBy: "reservable", cascade: [ "persist", "remove" ]),
        OrderBy([ "position" => "ASC" ]),
    ]
    private Collection $options;

    #[
        Column(type: Types::BOOLEAN, options: ["default" => false]),
        Expose
    ]
    private bool $hidden = false;

    public function __construct()
    {
        $this->groupPrices = new ArrayCollection();
        $this->options = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getForm(): ?Form
    {
        return $this->form;
    }

    public function setForm(Form $form): self
    {
        $this->form = $form;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function isSingleReservation(): bool
    {
        return $this->singleReservation;
    }

    public function setSingleReservation(bool $singleReservation): self
    {
        $this->singleReservation = $singleReservation;

        return $this;
    }

    public function getLimit(): ?int
    {
        return $this->limit;
    }

    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPriceType(): ?string
    {
        return $this->priceType;
    }

    public function setPriceType(string $priceType): self
    {
        $this->priceType = $priceType;

        return $this;
    }

    public function getSinglePrice(): ?float
    {
        return $this->singlePrice;
    }

    public function setSinglePrice(?float $singlePrice): self
    {
        $this->singlePrice = $singlePrice;

        return $this;
    }

    public function getStart(): ?\DateTime
    {
        return $this->start;
    }

    public function setStart(\DateTime $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?\DateTime
    {
        return $this->end;
    }

    public function setEnd(\DateTime $end): self
    {
        $this->end = $end;

        return $this;
    }

    public function getPageId(): ?string
    {
        return $this->pageId;
    }

    public function setPageId(string $pageId): self
    {
        $this->pageId = $pageId;

        return $this;
    }

    public function getParentPage(): ?string
    {
        return $this->parentPage;
    }

    public function setParentPage(string $parentPage): self
    {
        $this->parentPage = $parentPage;

        return $this;
    }

    /**
     * @return Collection|ReservableGroupPrice[]
     */
    public function getGroupPrices(): Collection
    {
        return $this->groupPrices;
    }

    public function setGroupPrices(Collection $groupPrices): self
    {
        foreach ($groupPrices as $groupPrice) {
            $this->addGroupPrice($groupPrice);
        }

        return $this;
    }

    public function addGroupPrice(ReservableGroupPrice $groupPrice): self {
        if (!$this->getGroupPrices()->contains($groupPrice)) {
            $groupPrice->setReservable($this);
            $this->groupPrices->add($groupPrice);
        }

        return $this;
    }

    public function removeGroupPrice(ReservableGroupPrice $groupPrice): self {
        if ($this->getGroupPrices()->contains($groupPrice)) {
            $groupPrice->setReservable(null);
            $this->groupPrices->removeElement($groupPrice);
        }

        return $this;
    }

    public function getGroupPricesForBlocks(): array
    {
        $blocks = [];
        /** @var ReservableGroupPrice $groupPrice */
        foreach ($this->groupPrices as $groupPrice) {
            $blocks[] = $groupPrice->toArray();
        }

        return $blocks;
    }

    public function getOptionsSelectionType(): ?string {
        return $this->optionSelectionType;
    }

    public function setOptionSelectionType(string $optionSelectionType): self {
        $this->optionSelectionType = $optionSelectionType;

        return $this;
    }

    /**
     * @return Collection|ReservableOption[]
     */
    public function getOptions(): Collection
    {
        return $this->options;
    }

    public function getOptionsAsBlock(): array {
        $array = [];

        foreach ($this->getOptions() as $option) {
            $array[] = $option->__toBlock();
        }

        return $array;
    }

    public function setOptions(Collection $options): self
    {
        foreach ($options as $option) {
            $this->addOption($option);
        }

        return $this;
    }

    public function addOption(ReservableOption $option): self {
        if (!$this->options->contains($option)) {
            $option->setReservable($this);
            $this->options->add($option);
        }

        return $this;
    }

    public function removeOption(ReservableOption $option): self {
        if ($this->options->contains($option)) {
            $this->options->removeElement($option);
        }

        return $this;
    }

    public function getTaxCategory(): ?TaxCategory {
        return $this->taxCategory;
    }

    public function setTaxCategory(?TaxCategory $taxCategory): self {
        $this->taxCategory = $taxCategory;

        return $this;
    }

    public function isHidden(): bool {
        return $this->hidden;
    }

    public function hide(): void {
        $this->hidden = true;
    }

    public function show(): void {
        $this->hidden = false;
    }

    #[ArrayShape([
        "id" => "int",
        "title" => "string",
        "limit" => "int",
        "type" => "string",
        "priceType" => "string",
        "singlePrice" => "float",
        "start" => DateTime::class,
        "end" => DateTime::class,
        "pageId" => "string",
        "parentPage" => "string",
        "groupPrices" => "array",
        "options" => "array",
        "formId" => "int",
        "taxCategory" => TaxCategory::class|null
    ])]
    public function toArray(): array
    {
        return [
            "id" => $this->getId(),
            "title" => $this->getTitle(),
            "limit" => $this->getLimit(),
            "type" => $this->getType(),
            "priceType" => $this->getPriceType(),
            "singlePrice" => $this->getSinglePrice(),
            "start" => $this->getStart(),
            "end" => $this->getEnd(),
            "pageId" => $this->getPageId(),
            "parentPage" => $this->getParentPage(),
            "groupPrices" => $this->getGroupPricesForBlocks(),
            "reservableOptions" => $this->getOptionsAsBlock(),
            "form" => $this->getForm()->getId(),
            "taxCategory" => $this->getTaxCategory()
        ];
    }
}
