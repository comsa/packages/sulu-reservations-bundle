<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Entity;

use Comsa\SuluReservations\Entity\Interfaces\CrudResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\InverseJoinColumn;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Sulu\Bundle\FormBundle\Entity\Dynamic;

#[
    Entity(),
    Table(name: "comsa_re_reservations"),
    ExclusionPolicy("all")
]
class Reservation implements CrudResource
{
    const RESOURCE_KEY = "comsa_re_reservations";

    #[
        Id(),
        GeneratedValue(strategy: "AUTO"),
        Column(type: Types::INTEGER),
        Expose()
    ]
    private ?int $id;

    #[
        ManyToOne(targetEntity: "Comsa\SuluReservations\Entity\Reservable"),
        JoinColumn(name: "reservable_id", referencedColumnName: "id"),
        Expose()
    ]
    private ?Reservable $reservable;

    #[
        OneToOne(targetEntity: "Comsa\SuluReservations\Entity\Payment"),
        JoinColumn(name: "payment_id", referencedColumnName: "id"),
        Expose()
    ]
    private ?Payment $payment;

    #[
        ManyToOne(targetEntity: "Sulu\Bundle\FormBundle\Entity\Dynamic"),
        JoinColumn(name: "form_data_id"),
        Expose()
    ]
    private ?Dynamic $formdata;

    #[
        Column(type: Types::INTEGER),
        Expose()
    ]
    private ?int $amount;

    #[
        Column(type: Types::DECIMAL, precision: 10, scale: 2),
        Expose()
    ]
    private ?float $price;

    #[
        ManyToMany(targetEntity: "Comsa\SuluReservations\Entity\Option"),
        JoinTable(name: "comsa_re_reservation_options"),
        JoinColumn(name: "reservation_id", referencedColumnName: "id"),
        InverseJoinColumn(name: "option_id", referencedColumnName: "id"),
        Expose()
    ]
    private Collection $options;

    #[
        OneToMany(targetEntity: "Comsa\SuluReservations\Entity\Group", mappedBy: "reservation", cascade: ["persist"]),
        Expose()
    ]
    private Collection $groups;

    public function __construct() {
        $this->options = new ArrayCollection();
        $this->groups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReservable(): ?Reservable
    {
        return $this->reservable;
    }

    public function setReservable(Reservable $reservable): self
    {
        $this->reservable = $reservable;

        return $this;
    }

    public function getPayment(): ?Payment
    {
        return $this->payment;
    }

    public function setPayment(?Payment $payment): self
    {
        $this->payment = $payment;

        return $this;
    }

    public function getFormdata(): ?Dynamic
    {
        return $this->formdata;
    }

    public function setFormdata(Dynamic $formdata): self
    {
        $this->formdata = $formdata;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getOptions(): Collection
    {
        return $this->options;
    }

    public function setOptions(Collection $options): self
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @return Collection|Group[]
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    public function setGroups(Collection $groups): self {
        $this->groups = $groups;

        return $this;
    }

    public function addGroup(Group $group): self {
        if (!$this->getGroups()->contains($group)) {
            $group->setReservation($this);
            $this->groups->add($group);
        }

        return $this;
    }

    public function removeGroup(Group $group): self {
        if ($this->getGroups()->contains($group)) {
            $this->groups->remove($group);
        }

        return $this;
    }

}
