<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Entity;

use Comsa\SuluReservations\Entity\Interfaces\CrudResource;
use Comsa\SuluReservations\Entity\Reservation;
use Comsa\SuluReservations\Entity\Traits\TimestampableTrait;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

#[
    Entity(),
    Table(name: "comsa_re_payments"),
    HasLifecycleCallbacks(),
    ExclusionPolicy("all")
]
class Payment implements CrudResource
{
    use TimestampableTrait;

    #[
        Id(),
        GeneratedValue(strategy: "AUTO"),
        Column(type: Types::INTEGER),
        Expose()
    ]
    private ?int $id;

    #[
        ManyToOne(targetEntity: "Comsa\SuluReservations\Entity\PaymentMethod"),
        JoinColumn(name: "payment_method_id"),
        Expose()
    ]
    private ?PaymentMethod $paymentMethod;

    #[
        Column(type: Types::DECIMAL, precision: 10, scale: 2),
        Expose()
    ]
    private ?float $amount;

    #[
        Column(type: Types::STRING, nullable: true),
        Expose()
    ]
    private ?string $externalId = null;

    #[
        Column(type: Types::DATETIME_MUTABLE, nullable: true),
        Expose()
    ]
    private ?\DateTime $paymentCompletedAt = null;

    #[
        Column(type: Types::BOOLEAN),
        Expose()
    ]
    private bool $refunded = false;

    public function getPaymentMethod(): ?PaymentMethod
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(PaymentMethod $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    public function setExternalId(string $externalId): self
    {
        $this->externalId = $externalId;

        return $this;
    }

    public function getPaymentCompletedAt(): ?\DateTime
    {
        return $this->paymentCompletedAt;
    }

    public function isPaid(): bool {
        return $this->getPaymentCompletedAt() !== null;
    }

    public function setPaid(): self
    {
        $this->paymentCompletedAt = new \DateTime();

        return $this;
    }

    public function isRefunded(): bool {
        return $this->refunded;
    }

    public function refund(): self {
        $this->refunded = true;

        return $this;
    }
}
