<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Entity\Traits;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\PreUpdate;

/**
 * @package Comsa\SuluReservations\Entity\Traits
 */
trait TimestampableTrait
{
    #[Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTime $createdAt;

    #[Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTime $updatedAt;

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    #[PrePersist()]
    public function setCreatedAt(): self
    {
        $this->createdAt = new \DateTime();
        $this->setUpdatedAt(new \DateTime());

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    #[PreUpdate()]
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }
}
