<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Entity;

use Comsa\SuluReservations\Entity\Interfaces\CrudResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

#[
    Entity(),
    Table(name: "comsa_re_reservation_groups"),
    ExclusionPolicy("all")
]
class Group implements CrudResource {

    #[
        Id(),
        GeneratedValue(strategy: "AUTO"),
        Column(type: Types::INTEGER),
        Expose()
    ]
    private ?int $id;

    #[
        ManyToOne(targetEntity: "Comsa\SuluReservations\Entity\GroupPrice"),
        JoinColumn(name: "group_id", referencedColumnName: "id"),
        Expose()
    ]
    private ?GroupPrice $group;

    #[
        ManyToOne(targetEntity: "Comsa\SuluReservations\Entity\Reservation", inversedBy: "groups"),
        JoinColumn(name: "reservation_id", referencedColumnName: "id"),
        Expose()
    ]
    private ?Reservation $reservation;

    #[
        Column(type: Types::INTEGER),
        Expose()
    ]
    private ?int $amount;

    #[
        Column(type: Types::DECIMAL, precision: 10, scale: 2),
        Expose()
    ]
    private ?float $price;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGroup(): ?GroupPrice
    {
        return $this->group;
    }

    public function setGroup(GroupPrice $group): self
    {
        $this->group = $group;

        return $this;
    }

    public function getReservation(): ?Reservation
    {
        return $this->reservation;
    }

    public function setReservation(Reservation $reservation): self
    {
        $this->reservation = $reservation;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }
}
