<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Entity;

use Comsa\SuluReservations\Entity\Interfaces\CrudResource;
use Comsa\SuluReservations\Utility\TypeConverter;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use JetBrains\PhpStorm\ArrayShape;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

#[
    Entity(),
    Table(name: "comsa_re_reservable_options"),
    ExclusionPolicy("all")
]
class ReservableOption
{
    #[
        Id(),
        GeneratedValue(strategy: "AUTO"),
        Column(type: Types::INTEGER),
        Expose()
    ]
    private ?int $id;

    #[
        ManyToOne(targetEntity: "Comsa\SuluReservations\Entity\Reservable", inversedBy: "options"),
        JoinColumn(name: "reservable_id", referencedColumnName: "id"),
        Expose()
    ]
    private ?Reservable $reservable;

    #[
        ManyToOne(targetEntity: "Comsa\SuluReservations\Entity\Option"),
        JoinColumn(name: "option_id", referencedColumnName: "id"),
        Expose()
    ]
    private ?Option $option;

    #[
        Column(type: Types::INTEGER),
        Expose()
    ]
    private ?int $position;

    public function getId(): ?int {
        return $this->id;
    }

    public function getReservable(): ?Reservable {
        return $this->reservable;
    }

    public function setReservable(?Reservable $reservable): self {
        $this->reservable = $reservable;

        return $this;
    }

    public function getOption(): ?Option {
        return $this->option;
    }

    public function setOption(Option $option): self {
        $this->option = $option;

        return $this;
    }

    public function getPosition(): ?int {
        return $this->position;
    }

    public function setPosition(int $position): self {
        $this->position = $position;

        return $this;
    }

    #[ArrayShape([
        "id" => "int",
        "title" => "string",
        "price" => "float"
    ])]
    public function toArray(): array
    {
        return [
            "id" => $this->option->getId(),
            "title" => $this->option->getTitle(),
            "price" => $this->option->getPrice()
        ];
    }

    #[ArrayShape([
        "type" => "string",
        "optionTitle" => "string",
        "optionPrice" => "string",
        "optionPosition" => "int"
    ])]
    public function __toBlock(): array {
        return [
            "type" => "reservable_option",
            "optionTitle" => $this->option->getTitle(),
            "optionPrice" => TypeConverter::floatToString($this->getOption()->getPrice()),
            "optionPosition" => $this->getPosition()
        ];
    }

    public function __toString(): string {
        return sprintf("%s%d", $this->getOption()->getTitle(), $this->getOption()->getPrice());
    }
}
