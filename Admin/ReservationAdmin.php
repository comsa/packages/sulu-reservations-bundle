<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Admin;

use Comsa\SuluReservations\Entity\ActivityPeriod;
use Comsa\SuluReservations\Entity\Exception;
use Comsa\SuluReservations\Entity\Interval;
use Comsa\SuluReservations\Entity\Location;
use Comsa\SuluReservations\Entity\Option;
use Comsa\SuluReservations\Entity\PersonGroup;
use Comsa\SuluReservations\Entity\Reservable;
use Comsa\SuluReservations\Entity\Reservation;
use Comsa\SuluReservations\Entity\Setting;
use Comsa\SuluReservations\Entity\TaxCategory;
use FOS\RestBundle\View\View;
use Sulu\Bundle\AdminBundle\Admin\Admin;
use Sulu\Bundle\AdminBundle\Admin\Navigation\NavigationItem;
use Sulu\Bundle\AdminBundle\Admin\Navigation\NavigationItemCollection;
use Sulu\Bundle\AdminBundle\Admin\View\ToolbarAction;
use Sulu\Bundle\AdminBundle\Admin\View\ViewBuilder;
use Sulu\Bundle\AdminBundle\Admin\View\ViewBuilderFactoryInterface;
use Sulu\Bundle\AdminBundle\Admin\View\ViewCollection;
use Sulu\Component\Webspace\Manager\WebspaceManagerInterface;

/**
 * Configures the Sulu Admin to implement the Reservations Module
 * @package Comsa\SuluReservations\Admin
 */
class ReservationAdmin extends Admin
{
    const RESERVATION_LIST_KEY = "comsa_re_reservations";
    const RESERVATION_LIST_VIEW = "comsa_sulu_reservations.reservation_list";
    const RESERVATIONS_EDIT_VIEW = "comsa_sulu_reservations.reservation_edit";
    const RESERVATIONS_FORM_KEY = "comsa_re_reservation_details";

    const RESERVABLE_LIST_KEY = "comsa_re_reservables";
    const RESERVABLE_LIST_VIEW = "comsa_sulu_reservations.reservable_list";
    const RESERVABLE_EDIT_VIEW = "comsa_sulu_reservations.reservable_edit";
    const RESERVABLE_FORM_KEY = "comsa_re_reservable_details";
    const RESERVABLE_ADD_VIEW = "comsa_sulu_reservations.reservable_add";

    const TAX_CATEGORY_LIST_KEY = "comsa_re_tax_categories";
    const TAX_CATEGORY_LIST_VIEW = "comsa_sulu_reservations.tax_category_list";
    const TAX_CATEGORY_EDIT_VIEW = "comsa_sulu_reservations.tax_category_edit";
    const TAX_CATEGORY_FORM_KEY = "comsa_re_tax_category_details";
    const TAX_CATEGORY_ADD_VIEW = "comsa_sulu_reservations.tax_category_add";

    const OPTION_LIST_KEY = "comsa_re_options";
    const OPTION_LIST_VIEW = "comsa_sulu_reservations.option_list";
    const OPTION_EDIT_VIEW = "comsa_sulu_reservations.option_edit";
    const OPTION_FORM_KEY = "comsa_re_option_details";
    const OPTION_ADD_VIEW = "comsa_sulu_reservations.option_add";

    const SETTING_LIST_KEY = "comsa_re_settings";
    const SETTING_LIST_VIEW = "comsa_sulu_reservations.setting_list";
    const SETTING_EDIT_VIEW = "comsa_sulu_reservations.setting_edit";
    const SETTING_FORM_KEY = "comsa_re_setting_details";

    private ViewBuilderFactoryInterface $viewBuilderFactory;

    public function __construct(ViewBuilderFactoryInterface $viewBuilderFactory)
    {
        $this->viewBuilderFactory = $viewBuilderFactory;
    }

    public function configureNavigationItems(NavigationItemCollection $navigationItemCollection): void
    {
        $module = new NavigationItem("comsa_sulu_reservations.reservation_module");
        $module->setIcon("su-desktop");

        $reservationItem = new NavigationItem("comsa_sulu_reservations.reservations");
        $reservationItem->setView(self::RESERVATION_LIST_VIEW);
        $reservationItem->setPosition(1);

        $reservableItem = new NavigationItem("comsa_sulu_reservations.reservables");
        $reservableItem->setView(self::RESERVABLE_LIST_VIEW);
        $reservableItem->setPosition(2);

        $taxCategoriesItem = new NavigationItem("comsa_sulu_reservations.tax_categories");
        $taxCategoriesItem->setView(self::TAX_CATEGORY_LIST_VIEW);
        $taxCategoriesItem->setPosition(3);

        $settingItem = new NavigationItem("comsa_sulu_reservations.settings");
        $settingItem->setView(self::SETTING_LIST_VIEW);
        $settingItem->setPosition(4);

        $module->addChild($reservationItem);
        $module->addChild($reservableItem);
        $module->addChild($taxCategoriesItem);
        $module->addChild($settingItem);

        $navigationItemCollection->add($module);
    }

    private function configureReservationViews(ViewCollection $viewCollection): void
    {
        $reservationListView = $this->viewBuilderFactory->createListViewBuilder(
            self::RESERVATION_LIST_VIEW,
            "/reservations/reservations"
        )
            ->setResourceKey(Reservation::RESOURCE_KEY)
            ->setListKey(self::RESERVATION_LIST_KEY)
            ->setTitle("comsa_sulu_reservations.reservations")
            ->addListAdapters(["table"])
            ->setEditView(self::RESERVATIONS_EDIT_VIEW)
            ->addToolbarActions([
                new ToolbarAction("sulu_admin.delete")
            ]);

        $viewCollection->add($reservationListView);

        $reservationEditView = $this->viewBuilderFactory->createViewBuilder(
            self::RESERVATIONS_EDIT_VIEW,
            "/reservations/reservations/:id",
            "sulu_reservations.reservation_overview"
        );

        $viewCollection->add($reservationEditView);
    }

    private function configureReservableViews(ViewCollection $viewCollection): void
    {
        $reservableListView = $this->viewBuilderFactory->createListViewBuilder(
            self::RESERVABLE_LIST_VIEW,
            "/reservations/reservables"
        )
            ->setResourceKey(Reservable::RESOURCE_KEY)
            ->setListKey(self::RESERVABLE_LIST_KEY)
            ->setTitle("comsa_sulu_reservations.reservables")
            ->setAddView(self::RESERVABLE_ADD_VIEW)
            ->setEditView(self::RESERVABLE_EDIT_VIEW)
            ->addToolbarActions([
                new ToolbarAction("sulu_admin.add"),
                new ToolbarAction("sulu_admin.delete")
            ])
            ->addListAdapters(["table"]);

        $viewCollection->add($reservableListView);

        $addReservableResourceTab = $this->viewBuilderFactory->createResourceTabViewBuilder(
            self::RESERVABLE_ADD_VIEW,
            "/reservations/reservable/add"
        )
            ->setResourceKey(Reservable::RESOURCE_KEY)
            ->setBackView(self::RESERVABLE_LIST_VIEW);

        $viewCollection->add($addReservableResourceTab);

        $addReservableView = $this->viewBuilderFactory->createFormViewBuilder(
            self::RESERVABLE_ADD_VIEW . ".details",
            "/reservations/details"
        )
            ->setResourceKey(Reservable::RESOURCE_KEY)
            ->setFormKey(self::RESERVABLE_FORM_KEY)
            ->setTabTitle("sulu_admin.details")
            ->setEditView(self::RESERVABLE_EDIT_VIEW)
            ->addToolbarActions([
                new ToolbarAction("sulu_admin.save")
            ])
            ->setParent(self::RESERVABLE_ADD_VIEW);

        $viewCollection->add($addReservableView);

        $editReservableResourceTab = $this->viewBuilderFactory->createResourceTabViewBuilder(
            self::RESERVABLE_EDIT_VIEW,
            "/reservations/reservables/:id"
        )
            ->setResourceKey(Reservable::RESOURCE_KEY)
            ->setBackView(self::RESERVABLE_LIST_VIEW)
            ->setTitleProperty("title");

        $viewCollection->add($editReservableResourceTab);

        $editReservableView = $this->viewBuilderFactory->createFormViewBuilder(
            self::RESERVABLE_EDIT_VIEW . ".details",
            "/reservations/reservables/details"
        )
            ->setResourceKey(Reservable::RESOURCE_KEY)
            ->setFormKey(self::RESERVABLE_FORM_KEY)
            ->setTabTitle("sulu_admin.details")
            ->addToolbarActions([
                new ToolbarAction("sulu_admin.save"),
                new ToolbarAction("sulu_admin.delete")
            ])
            ->setParent(self::RESERVABLE_EDIT_VIEW);

        $viewCollection->add($editReservableView);

        $reservationsForReservableView = $this->viewBuilderFactory->createListViewBuilder(
            self::RESERVABLE_EDIT_VIEW . ".reservations",
            "/reservations/reservables/reservations"
        )
            ->setResourceKey(Reservation::RESOURCE_KEY)
            ->setListKey(self::RESERVATION_LIST_KEY)
            ->setTabTitle("comsa_sulu_reservations.reservations")
            ->addListAdapters(["table"])
            ->addRouterAttributesToListRequest(["id" => "id"])
            ->addResourceStorePropertiesToListRequest(["id" => "id"])
            ->addToolbarActions([
                new ToolbarAction("comsa_sulu_reservations.export_reservations")
            ])
            ->setParent(self::RESERVABLE_EDIT_VIEW)
        ;

        $viewCollection->add($reservationsForReservableView);
    }

    private function configureTaxCategoryViews(ViewCollection $viewCollection) : void {
        // List view
        $taxListView = $this->viewBuilderFactory->createListViewBuilder(
            self::TAX_CATEGORY_LIST_VIEW,
            "/reservations/tax-categories"
        )
            ->setResourceKey(TaxCategory::RESOURCE_KEY)
            ->setListKey(self::TAX_CATEGORY_LIST_KEY)
            ->setTitle("comsa_sulu_reservations.tax_categories")
            ->setAddView(self::TAX_CATEGORY_ADD_VIEW)
            ->setEditView(self::TAX_CATEGORY_EDIT_VIEW)
            ->addToolbarActions([
                new ToolbarAction("sulu_admin.add"),
                new ToolbarAction("sulu_admin.delete")
            ])
            ->addListAdapters(["table"])
        ;

        $viewCollection->add($taxListView);

        // Add Resource tab
        $viewCollection->add(
            $this->viewBuilderFactory->createResourceTabViewBuilder(
                self::TAX_CATEGORY_ADD_VIEW,
                "/reservations/tax-categories/add"
            )
            ->setResourceKey(TaxCategory::RESOURCE_KEY)
            ->setBackView(self::TAX_CATEGORY_LIST_VIEW)
        );

        // Add view
        $addView = $this->viewBuilderFactory->createFormViewBuilder(
            self::TAX_CATEGORY_ADD_VIEW . ".details",
            "/reservations/details"
        )
            ->setResourceKey(TaxCategory::RESOURCE_KEY)
            ->setFormKey(self::TAX_CATEGORY_FORM_KEY)
            ->setTabTitle("sulu_admin.details")
            ->setEditView(self::TAX_CATEGORY_EDIT_VIEW)
            ->addToolbarActions([
                new ToolbarAction("sulu_admin.save")
            ])
            ->setParent(self::TAX_CATEGORY_ADD_VIEW)
        ;

        $viewCollection->add($addView);

        // Edit resource tab
        $viewCollection->add(
            $this->viewBuilderFactory->createResourceTabViewBuilder(
                self::TAX_CATEGORY_EDIT_VIEW,
                "/reservations/tax-categories/:id"
            )
            ->setResourceKey(TaxCategory::RESOURCE_KEY)
            ->setBackView(self::TAX_CATEGORY_LIST_VIEW)
            ->setTitleProperty("title")
        );

        // Edit view
        $editView = $this->viewBuilderFactory->createFormViewBuilder(
            self::TAX_CATEGORY_EDIT_VIEW . ".details",
            "/reservations/details"
        )
            ->setResourceKey(TaxCategory::RESOURCE_KEY)
            ->setFormKey(self::TAX_CATEGORY_FORM_KEY)
            ->setTabTitle("sulu_admin.details")
            ->addToolbarActions([
                new ToolbarAction("sulu_admin.save"),
                new ToolbarAction("sulu_admin.delete")
            ])
            ->setParent(self::TAX_CATEGORY_EDIT_VIEW)
        ;

        $viewCollection->add($editView);
    }

    private function configureSettingViews(ViewCollection $viewCollection): void
    {
        $settingListView = $this->viewBuilderFactory->createListViewBuilder(
            self::SETTING_LIST_VIEW,
            "/reservations/settings"
        )
            ->setResourceKey(Setting::RESOURCE_KEY)
            ->setListKey(self::SETTING_LIST_KEY)
            ->setTitle("comsa_sulu_reservations.settings")
            ->addListAdapters(["table"])
            ->setEditView(self::SETTING_EDIT_VIEW);

        $viewCollection->add($settingListView);

        $editSettingResourceTab = $this->viewBuilderFactory->createResourceTabViewBuilder(
            self::SETTING_EDIT_VIEW,
            "/reservations/settings/:id"
        )
            ->setResourceKey(Setting::RESOURCE_KEY)
            ->setBackView(self::SETTING_LIST_VIEW)
            ->setTitleProperty("title");

        $viewCollection->add($editSettingResourceTab);

        $editSettingView = $this->viewBuilderFactory->createFormViewBuilder(
            self::SETTING_EDIT_VIEW . ".details",
            "/reservations/details"
        )
            ->setResourceKey(Setting::RESOURCE_KEY)
            ->setFormKey(self::SETTING_FORM_KEY)
            ->setTabTitle("sulu_admin.details")
            ->addToolbarActions([
                new ToolbarAction("sulu_admin.save")
            ])
            ->setParent(self::SETTING_EDIT_VIEW);

        $viewCollection->add($editSettingView);
    }


    public function configureViews(ViewCollection $viewCollection): void
    {
        $this->configureReservationViews($viewCollection);
        $this->configureReservableViews($viewCollection);
        $this->configureTaxCategoryViews($viewCollection);
        $this->configureSettingViews($viewCollection);
    }
}
