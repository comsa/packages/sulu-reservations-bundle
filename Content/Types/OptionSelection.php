<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Content\Types;

use Comsa\SuluReservations\Entity\Option;
use Doctrine\ORM\EntityManagerInterface;
use Sulu\Component\Content\ComplexContentType;
use PHPCR\NodeInterface;
use Sulu\Component\Content\Compat\PropertyInterface;

/**
 * Content Type for Options
 * @package Comsa\SuluReservations\Content\Types
 */
class OptionSelection extends ComplexContentType
{
    protected EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function read(NodeInterface $node, PropertyInterface $property, $webspacekey, $languageCode, $segmentKey)
    {
        $optionId = $node->getPropertyValueWithDefault($property->getName(), []);
        $property->setValue($optionId);
    }

    public function write(NodeInterface $node, PropertyInterface $property, $userId, $webspaceKey, $languageCode, $segmentCode)
    {
        $optionIds = [];
        $value = $property->getValue();

        if (!$value) {
            $node->setProperty($property->getName(), null);
            return;
        }

        foreach ($value as $option) {
            if (is_numeric($option)) {
                $optionIds[] = $option;
            } else {
                $optionIds[] = $option["id"];
            }
        }

        $node->setProperty($property->getName(), $optionIds);
    }

    public function remove(NodeInterface $node, PropertyInterface $property, $webspaceKey, $languageCode, $segmentKey)
    {
        if ($node->hasProperty($property->getName())) {
            $property = $node->getProperty($property->getName());
            $property->remove();
        }
    }

    public function getContentData(PropertyInterface $property)
    {
        $optionIds = $property->getValue();
        foreach ($optionIds as $optionId) {
            $option = $this->entityManager->getRepository(Option::class)->find($option);
        }

        return $optionIds;
    }
}
