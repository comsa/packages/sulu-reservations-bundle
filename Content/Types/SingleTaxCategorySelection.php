<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Content\Types;

use Comsa\SuluReservations\Entity\TaxCategory;
use Comsa\SuluReservations\Repository\TaxCategoryRepository;
use Sulu\Component\Content\Compat\PropertyInterface;
use Sulu\Component\Content\SimpleContentType;

class SingleTaxCategorySelection extends SimpleContentType {
    private TaxCategoryRepository $taxCategoryRepository;

    public function __construct(TaxCategoryRepository $taxCategoryRepository) {
        $this->taxCategoryRepository = $taxCategoryRepository;

        parent::__construct("comsa.re.single_selection.tax_category", null);
    }

    public function getContentData(PropertyInterface $property): ?TaxCategory {
        $taxCategoryId = $property->getValue();

        if (!$taxCategoryId) {
            return null;
        }

        return $this->taxCategoryRepository->find($taxCategoryId);
    }
}
