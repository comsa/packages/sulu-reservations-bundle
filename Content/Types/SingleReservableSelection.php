<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Content\Types;

use Comsa\SuluReservations\Entity\Reservable;
use Doctrine\ORM\EntityManagerInterface;
use Sulu\Component\Content\Compat\PropertyInterface;
use Sulu\Component\Content\SimpleContentType;

/**
 * Content Type for Reservables
 * @package Comsa\SuluReservations\Content\Types
 */
class SingleReservableSelection extends SimpleContentType
{
    protected EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct("single_reservable_selection", null);
    }

    public function getContentData(PropertyInterface $property)
    {
        $reservableId = $property->getValue();
        if (!$reservableId) {
            return null;
        }

        $reservable = $this->entityManager->getRepository(Reservable::class)->find($reservableId);

        return $reservable;
    }
}
