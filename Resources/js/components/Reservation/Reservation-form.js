import Vue from 'vue/dist/vue.js';
import moment from 'moment';
import axios from 'axios';

let reservationForm = new Vue({
  el: '#reservationForm',
  delimiters: ['${', '}'],
  async created() {
    let reservableId = document.getElementById('reservableId').value;
    this.form.reservable = reservableId;
    await axios.get(`/sulu-reservations/get/reservable/${reservableId}`).then((response) => {
      this.reservable = response.data;
      this.spacesLeft = this.reservable.spacesLeft;
    })

    //-- Skip step one (choose amount if single_payment)
    if (this.reservable.priceType == 'SINGLE_PAYMENT') {
      this.form.total = 1;
      this.nextStep();
    }

    //-- Skip step two if no options
    if (this.reservable.reservableOptions.length <= 0) {
      this.nextStep();
    }

    await axios.get(`/sulu-reservations/get/group-prices/${reservableId}`).then((response) => {
      this.form.groupPrices = response.data;
      this.loading = false;
    });

  },
  data() {
    return {
      totalPrice: 0.00,
      spacesLeft: 0,
      loading: true,
      invalidAmount: false,
      step: 1,
      reservable: {},
      form: {
        groupPrices: [],
        options: [],
        optionObjects: [],
      }
    }
  },
  methods: {
    nextStep() {
      if (this.step == 1) {
        if (this.reservable.priceType == "PER_PERSON") {
          if (this.form.total <= 0 || this.form.total > this.spacesLeft) {
            this.invalidAmount = true;
            return;
          }
        }
      }

      this.calculateTotalPrice();

      this.step += 1;
    },
    previousStep() {
      this.step = this.step - 1;
    },
    calculateTotalVisitors() {
      let total = 0;

      for (let groupPrice of this.form.groupPrices) {
        total = parseInt(total) + parseInt(groupPrice.amount);
      }

      this.form.total = total;
    },
    async calculateTotalPrice() {
      this.loading = true;

      let total = 0;

      if (this.reservable.priceType !== "SINGLE_PAYMENT") {
        for (let groupPrice of this.form.groupPrices) {
          total += (parseInt(groupPrice.groupPrice) * parseInt(groupPrice.amount));
        }
      } else {
        total += parseInt(this.reservable.singlePrice);
      }

      let input = document.getElementsByName('options[]');

      for (let i = 0; i < input.length; i++) {
        if (input[i].checked) {
          if (!this.form.options.includes(input[i].value)){
            this.form.options.push(input[i].value);
          }
        } else  {
          if ( this.form.options.includes(input[i].value) ) {
            let index = this.form.options.indexOf(input[i].value);
            this.form.options.splice(index, 1);
          }
        }
      }

      this.form.optionObjects = [];

      for (let v = 0; v < this.form.options.length; v++) {
        let id = this.form.options[v];
        await axios.get(`/sulu-reservations/get/option/${id}`).then((response) => {
          this.form.optionObjects.push(response.data);
        });
      }

      for (let option of this.form.optionObjects) {
        if (this.form.total > 0) {
          total += parseInt(option.price) * this.form.total;
        }
      }

      this.totalPrice = total.toFixed(2);

      this.loading = false;
    },
  }
});
