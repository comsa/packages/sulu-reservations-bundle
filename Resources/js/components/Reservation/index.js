import { viewRegistry } from 'sulu-admin-bundle/containers/ViewRenderer';
import initializer from 'sulu-admin-bundle/services/initializer';
import Reservation from './Reservation';

initializer.addUpdateConfigHook('sulu_admin', (config: Object, initialized: boolean) => {
  if (!initialized) {
    viewRegistry.add('sulu_reservations.reservation', Reservation);
  }
});
