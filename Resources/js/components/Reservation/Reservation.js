import {observer} from 'mobx-react';
import {action,observable} from 'mobx';
import React from 'react';
import {ResourceRequester} from 'sulu-admin-bundle/services';
import {withToolbar} from 'sulu-admin-bundle/containers/Toolbar';
import Loader from 'sulu-admin-bundle/components/Loader';
import Section from 'sulu-admin-bundle/components/Form/Section'
import Card from 'sulu-admin-bundle/components/Card'

type Props = {
  ...ViewProps,
  title?: string,
};

class Reservation extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
    this.state = {
      reservation: null
    }
  }
  @action async componentDidMount() {
    const {router} = this.props;
    const reservation = await ResourceRequester.get('reservations', {id: router.attributes.id});
    this.setState({
      reservation
    })
  }
  render() {
    const { reservation } = this.state;
    if (!reservation) {
      return <Loader/>
    }
    return (
      <div>
        <Section label="Reservatie">
          <Card>
            <h1>Reservatie voor {reservation.date}</h1>
            {reservation.reservables.map((reservable) => (
              <div>{reservable.title}</div>
            ))}
            <div>{reservation.interval.title}</div>
          </Card>
        </Section>
      </div>
    )
  }
}

export default withToolbar(Reservation, function() {
  return {};
});
