import './components/Reservation/index';
import {formToolbarActionRegistry} from 'sulu-admin-bundle/views/Form';
import AbstractFormToolbarAction from 'sulu-admin-bundle/views/Form/ToolbarActions/AbstractFormToolbarAction';
import {translate} from 'sulu-admin-bundle/utils/Translator';
import router from 'sulu-admin-bundle/services/Router';
import {listToolbarActionRegistry} from 'sulu-admin-bundle/views';
import initializer from 'sulu-admin-bundle/services/initializer';
import { viewRegistry } from 'sulu-admin-bundle/containers/ViewRenderer';
import Reservation from "./views/Reservation/Reservation";
import ExportReservations from "./listToolbarActions/ExportReservations";

listToolbarActionRegistry.add('comsa_sulu_reservations.export_reservations', ExportReservations);

initializer.addUpdateConfigHook('sulu_admin', (config: Object, initialized: boolean) => {
  if (!initialized) {
    viewRegistry.add('sulu_reservations.reservation_overview', Reservation);
  }
});
