import {translate} from 'sulu-admin-bundle/utils';
import {AbstractListToolbarAction} from 'sulu-admin-bundle/views';
import symfonyRouting from 'fos-jsrouting/router';

export default class exportComments extends AbstractListToolbarAction {
  getToolbarItemConfig() {
    const {disable_for_empty_selection: disableForEmptySelection = false} = this.options;

    return {
      type: 'button',
      label: translate('comsa_sulu_reservations.go_to_calender'),
      icon: 'su-calendar',
      onClick: this.handleClick,
    };
  }

  handleClick = () => {
    window.location.replace(symfonyRouting.generate('comsa_sulu_reservations_reservations_overview'));
  }


  getYear(){
    let year = new Date().getFullYear();
    return year;
  }


}
