import React from 'react';
import type {ViewProps} from 'sulu-admin-bundle/containers/ViewRenderer';
import {withToolbar} from 'sulu-admin-bundle/containers/Toolbar';
import {ResourceRequester} from 'sulu-admin-bundle/services';
import {action, computed, toJS, isObservableArray, observable} from 'mobx';
import Loader from 'sulu-admin-bundle/components/Loader';
import Section from 'sulu-admin-bundle/components/Form/Section'
import Table from 'sulu-admin-bundle/components/Table'
import Grid from 'sulu-admin-bundle/components/Grid'
import Card from 'sulu-admin-bundle/components/Card'
import Divider from 'sulu-admin-bundle/components/Divider'
import {translate} from 'sulu-admin-bundle/utils/Translator';
import axios from 'axios';
import symfonyRouting from 'fos-jsrouting/router';

type
Props = {
  ...ViewProps,
  title?: string,
};

class Reservation extends React.Component<Props> {
  @observable loading = false;

  constructor(props: Props) {
    super(props);
    this.state = {
      reservation: null,
      formData: null
    }
  }

  @action
  async componentDidMount() {
    const {router} = this.props;
    const reservation = await ResourceRequester.get('comsa_re_reservations', {id: router.attributes.id});
    const formData = JSON.parse(reservation.formdata.data);

    this.setState({
      reservation
    })
  }

  @action
  async setPaid() {
    this.loading = true;

    const { router } = this.props;
    let id = router.attributes.id;

    const response = axios.put(
      symfonyRouting.generate(
          'comsa_sulu_reservations.put_reservation_paid',
          { id: id }
        )
    )
      .finally(action( () => {
        this.loading = false;
      }));

    window.location.reload();
  }

  @action
  async getReservation() {
    let {router} = this.props;
    let reservation = await ResourceRequester.get('comsa_re_reservations', {id: router.attributes.id});
    return reservation;
  }


  render() {
    const {reservation} = this.state;
    if (!reservation) {
      return <Loader />;
    }

    const formFields = reservation.formdata.form.fields.sort((a,b) => (a.order > b.order) ? 1 : -1);
    const formData = JSON.parse(reservation.formdata.data);

    const marginYStyle = {
      marginTop: "2rem",
      marginBottom: "5rem"
    };

    let isSinglePayment = false;

    if (reservation.reservable.priceType === 'SINGLE_PAYMENT') {
      isSinglePayment = true;
    }

    return (
      <div>
        <section label={translate('comsa_sulu_reservations.reservation')}>
          <Grid>
            <Grid.Item>
              <Card>
                <h1>{translate('comsa_sulu_reservations.reservation')} #{reservation.id}</h1>
                <h3>{translate('comsa_sulu_reservations.reservation_info')}</h3>
                <Table skin="light">
                  <Table.Header>
                    <Table.HeaderCell>{translate('comsa_sulu_reservations.reservable')}</Table.HeaderCell>
                    <Table.HeaderCell>{translate('comsa_sulu_reservations.group_people')}</Table.HeaderCell>
                    <Table.HeaderCell>{translate('comsa_sulu_reservations.amount_people')}</Table.HeaderCell>
                    <Table.HeaderCell>{translate('comsa_sulu_reservations.price')}</Table.HeaderCell>
                  </Table.Header>
                  <Table.Body>
                    {isSinglePayment ?
                      <Table.Row>
                        <Table.Cell>{reservation.reservable.title}</Table.Cell>
                        <Table.Cell colSpan="2">{translate('comsa_sulu_reservations.fixed_price')}</Table.Cell>
                        <Table.Cell>€{reservation.reservable.singlePrice.toFixed(2)}</Table.Cell>
                      </Table.Row>
                      :
                      reservation.groups.map((item) => (
                        <Table.Row key={item.id}>
                          <Table.Cell>{reservation.reservable.title} (€{item.price.toFixed(2)})</Table.Cell>
                          <Table.Cell>{item.group.groupName}</Table.Cell>
                          <Table.Cell>{item.amount}</Table.Cell>
                          <Table.Cell>€{(item.price * item.amount).toFixed(2)}</Table.Cell>
                        </Table.Row>
                      ))
                    }
                    {
                      isSinglePayment ?
                        reservation.options.map((item) => (
                          <Table.Row key={item.id}>
                            <Table.Cell colSpan="3">{item.title} (€{item.price.toFixed(2)})</Table.Cell>
                            <Table.Cell>€{(item.price * reservation.amount).toFixed(2)}</Table.Cell>
                          </Table.Row>
                        ))
                        :
                        reservation.options.map((item) => (
                          <Table.Row key={item.id}>
                            <Table.Cell colSpan="2">{item.title} (€{item.price.toFixed(2)})</Table.Cell>
                            <Table.Cell colSpan="1">{reservation.amount}</Table.Cell>
                            <Table.Cell>€{(item.price * reservation.amount).toFixed(2)}</Table.Cell>
                          </Table.Row>
                        ))
                    }
                    <Table.Row>
                      <Table.Cell colSpan="2">
                        <strong>{translate('comsa_sulu_reservations.payment_method')}</strong>
                      </Table.Cell>
                      <Table.Cell>
                        {reservation.payment ? reservation.payment.paymentMethod.name : 'Geen betalingsmethode gekozen.'}
                      </Table.Cell>
                      <Table.Cell>
                        €{reservation.payment ? reservation.payment.paymentMethod.price.toFixed(2) : 0.00}
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell colSpan="3"><strong>{translate('comsa_sulu_reservations.total_price')}</strong></Table.Cell>
                      <Table.Cell>€{reservation.price.toFixed(2)}</Table.Cell>
                    </Table.Row>
                  </Table.Body>
                </Table>
              </Card>
            </Grid.Item>
          </Grid>
        </section>
        <section label={translate('comsa_sulu_reservations.payment_status')}>
          <Grid>
            <Grid.Item>
              <Card>
                <h3>{translate('comsa_sulu_reservations.payment_status')}</h3>
                {reservation.payment ?
                  <p>
                    <strong>{translate('comsa_sulu_reservations.paid_at')}: </strong>
                    {reservation.payment.paymentCompletedAt ? reservation.payment.paymentCompletedAt.replace('T', ' ') : translate('comsa_sulu_reservations.not_paid')}
                  </p>
                  : translate('comsa_sulu_reservations.no_payment_linked')
                }
                {reservation.payment ?
                  <p>
                    <strong>{translate('comsa_sulu_reservations.refunded')}: </strong>
                    {reservation.payment.refunded ? translate('comsa_sulu_reservations.yes') : translate('comsa_sulu_reservations.no')}
                  </p>
                  : translate('comsa_sulu_reservations.no_payment_linked')
                }
              </Card>
            </Grid.Item>
          </Grid>
        </section>
        <section label={translate('comsa_sulu_reservations.form_data')} style={marginYStyle}>
          <Grid>
            <Grid.Item>
              <Card>
                <h1>{translate('comsa_sulu_reservations.form_data')}</h1>
                <Table skin="light">
                  <Table.Header>
                    {formFields.map((field) => (
                      <Table.HeaderCell>{
                        formData[field.key] !== '' ?
                          field.translations[0].title : null
                      }</Table.HeaderCell>
                    ))}
                  </Table.Header>
                  <Table.Body>
                    <Table.Row>
                      {formFields.map((field) => (
                        <Table.Cell>{typeof formData[field.key] == 'object' ?
                          formData[field.key].day + "-" + formData[field.key].month + "-" + formData[field.key].year :
                          formData[field.key]
                        }</Table.Cell>
                      ))}
                    </Table.Row>
                  </Table.Body>
                </Table>
              </Card>
            </Grid.Item>
          </Grid>
        </section>
      </div>
    )
  }
}

export default withToolbar(Reservation, function () {
  return {
    items: [
      {
        type: 'button',
        label: 'Markeer als betaald',
        icon: 'su-check',
        onClick: () => {
          this.setPaid()
        }
      }
    ]
  }
});
