<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Controller;

use Comsa\SuluReservations\Entity\Option;
use Comsa\SuluReservations\Entity\Reservable;
use Comsa\SuluReservations\Entity\ReservableGroupPrice;
use Comsa\SuluReservations\Entity\Reservation;
use Comsa\SuluReservations\Enum\SettingEnum;
use Comsa\SuluReservations\Manager\ReservationManager;
use Comsa\SuluReservations\PaymentMethods\DefaultType;
use Comsa\SuluReservations\PaymentMethods\MollieType;
use Comsa\SuluReservations\Service\OptionService;
use Comsa\SuluReservations\Service\ReservableService;
use Comsa\SuluReservations\Service\ReservationService;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Sulu\Bundle\FormBundle\Entity\Form;
use Sulu\Bundle\WebsiteBundle\Resolver\TemplateAttributeResolverInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Configures the public Controller for Reservations
 * @package Comsa\SuluReservations\Controller
 */
class ReservationController extends AbstractController {

    private EventDispatcherInterface $eventDispatcher;
    private SessionInterface $session;
    private ReservationManager $reservationManager;
    private ReservableService $reservableService;
    private ReservationService $reservationService;
    private EntityManagerInterface $entityManager;
    private OptionService $optionService;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        SessionInterface $session,
        ReservationManager $reservationManager,
        ReservableService $reservableService,
        ReservationService $reservationService,
        EntityManagerInterface $entityManager,
        OptionService $optionService

    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->session = $session;
        $this->reservationManager = $reservationManager;
        $this->reservableService = $reservableService;
        $this->reservationService = $reservationService;
        $this->entityManager = $entityManager;
        $this->optionService = $optionService;
    }

    public function thanks(TemplateAttributeResolverInterface $templateAttributeResolver, EntityManagerInterface $entityManager, MollieType $mollieType, DefaultType $defaultType): Response {
        $reservation = $this->reservationManager->get();
        $paymentMethod = $reservation->getPayment()->getPaymentMethod();

        if ($paymentMethod->getType() === SettingEnum::VALUES_PAYMENT_METHOD_MOLLIE) {
            $typeHandler = $mollieType;
        } else {
            $typeHandler = $defaultType;
        }

        $parameters = ["reservation" => $reservation];

        if (isset($typeHandler)) {
            $parameters = array_merge($parameters, $typeHandler->getAdditionalTemplateData($reservation));
        }

        $data = $templateAttributeResolver->resolve($parameters);

        if ($paymentMethod->getType() === SettingEnum::VALUES_PAYMENT_METHOD_MOLLIE) {
            return $this->render("@SuluReservations/paymentMethods/mollie.html.twig");
        } else {
            return $this->render("@SuluReservations/paymentMethods/default.html.twig", $data);
        }
    }

    public function getReservableById(int $id): Response {
        /** @var Reservable $reservable */
        $reservable = $this->reservableService->getRepository()->find($id);

        $data = $reservable->toArray();
        $data["spacesLeft"] = $this->reservableService->calculateSpacesLeft($reservable);

        return new JsonResponse($data);
    }

    public function getFormById(int $id): Response {
        /** @var Form $suluForm */
        $suluForm = $this->entityManager->getRepository(Form::class)->find($id);
        $data = [];

        //-- Prep array for Vue
        foreach ($suluForm->getFields() as $field) {
            $data["fields"][$field->getKey()] = "";
        }

        return new JsonResponse($data);
    }

    public function getOptionById(int $id) {
        /** @var Option $option */
        $option = $this->optionService->getRepository()->find($id);

        return new JsonResponse($option->toArray());
    }

    public function getPriceGroupsForReservable(int $id): Response {
        /** @var Reservable $reservable */
        $reservable = $this->reservableService->getRepository()->find($id);

        $data = [];

        /** @var ReservableGroupPrice $groupPrice */
        foreach ($reservable->getGroupPrices() as $groupPrice) {
            $data[] = $groupPrice->toArray();
        }

        return new JsonResponse($data);
    }

    public function calculatePrice(Reservable $reservable,Reservation $reservation, ?array $groups, ?array $options): float {
        return $this->reservationService->calculatePrice(
            $reservable,
            $reservation,
            $groups,
            $options
        );
    }

    public function export(Request $request): Response
    {
        $id = $request->query->get("id");

        /** @var Reservable $reservable */
        $reservable = $this->reservableService->getRepository()->find($id);
        $reservations = $this->reservationService->getRepository()->findByReservable($reservable);

        $spreadSheet = new Spreadsheet();
        $sheet = $spreadSheet->getActiveSheet();
        $sheet->setTitle("Reservaties");

        $sheet->setCellValue("A1", "Voornaam");
        $sheet->setCellValue("B1", "Naam");
        $sheet->setCellValue("C1", "Telefoon");
        $sheet->setCellValue("D1", "E-mail");
        $sheet->setCellValue("E1", "Geboortedatum");
        $sheet->setCellValue("F1", "Betaald op");


        $sheet->getStyle("A1:E1")->applyFromArray([
            "fill" => [
                "fillType" => Fill::FILL_SOLID,
                "startColor" => [
                    "argb" => "FFFF00"
                ]
            ]
        ]);

        $x = 2;
        /** @var Reservation $reservation */
        foreach ($reservations as $reservation) {
            $formFields = $reservation->getFormdata()->getFields();
            $sheet->setCellValue("A" . $x, $formFields["firstName"]);
            $sheet->setCellValue("B" . $x, $formFields["lastName"]);
            $sheet->setCellValue("C" . $x, $formFields["phone"]);
            $sheet->setCellValue("D" . $x, $formFields["email"]);
            $sheet->setCellValue("E" . $x, sprintf(
                "%s-%s-%s",
                $formFields["date"]["day"],
                $formFields["date"]["month"],
                $formFields["date"]["year"]
            ));
            if ($reservation->getPayment() && $reservation->getPayment()->getPaymentCompletedAt()) {
                $sheet->setCellValue("F" . $x, $reservation->getPayment()->getPaymentCompletedAt()->format("d-m-Y"));
            }

            $x++;
        }

        $writer = new Xlsx($spreadSheet);
        $fileName = sprintf("Inschrijvingen %s.xlsx", str_replace("/", "-", $reservable->getTitle()));
        $tempFile = tempnam(sys_get_temp_dir(), $fileName);
        $writer->save($tempFile);

        return $this->file($tempFile, $fileName);
    }
}
