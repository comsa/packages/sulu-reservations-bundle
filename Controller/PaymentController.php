<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Controller;

use Comsa\SuluReservations\Entity\Payment;
use Comsa\SuluReservations\Entity\PaymentMethod;
use Comsa\SuluReservations\Entity\Reservation;
use Comsa\SuluReservations\Entity\Setting;
use Comsa\SuluReservations\Enum\SettingEnum;
use Comsa\SuluReservations\Factory\PaymentFactory;
use Comsa\SuluReservations\Manager\ReservationManager;
use Comsa\SuluReservations\PaymentMethods\DefaultType;
use Comsa\SuluReservations\PaymentMethods\MollieType;
use Comsa\SuluReservations\Repository\PaymentRepository;
use Comsa\SuluReservations\Service\PaymentService;
use Comsa\SuluReservations\Service\ReservationService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Configures the public Controller for Payments
 * @package Comsa\SuluReservations\Controller
 */
class PaymentController extends AbstractController
{
    private ReservationManager $reservationManager;
    private PaymentService $paymentService;
    private ReservationService $reservationService;

    public function __construct(
        ReservationManager $reservationManager,
        PaymentService $paymentService,
        ReservationService $reservationService
    ) {
        $this->reservationManager = $reservationManager;
        $this->paymentService = $paymentService;
        $this->reservationService = $reservationService;
    }

    public function selectPaymentMethod(Request $request)
    {
        $reservation = $this->reservationManager->get();

        $form = $this->createFormBuilder()
            ->add("paymentMethod", EntityType::class, [
                "class" => PaymentMethod::class,
                "query_builder" => function (EntityRepository $entityRepository) {
                    return $entityRepository->createQueryBuilder("pm")
                        ->where("pm.hidden = false")
                    ;
                },
                "expanded" => true,
                "label" => false
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $totalPrice = $reservation->getPrice() + $form->get("paymentMethod")->getData()->getPrice();

            $payment = $this->paymentService->create([
                "paymentMethod" => $form->get("paymentMethod")->getData(),
                "amount" => $totalPrice
            ]);

            $this->reservationService->updatePayment($reservation, [
                "price" => $totalPrice,
                "payment" => $payment
            ]);

            return $this->redirectToRoute("comsa_sulu_reservations_handle_payment");
        }

        return $this->render("@SuluReservations/select-payment.html.twig", [
            "form" => $form->createView()
        ]);
    }

    public function handlePayment(Request $request, MollieType $mollieType, DefaultType $defaultType) {
        $reservation = $this->reservationManager->get();

        $paymentMethod = $reservation->getPayment()->getPaymentMethod()->getType();

        if ($paymentMethod === SettingEnum::VALUES_PAYMENT_METHOD_MOLLIE) {
            $typeHandler = $mollieType;
        } else {
            $typeHandler = $defaultType;
        }

        return $typeHandler->handlePayment($reservation->getPayment(), $reservation);
    }

    public function handleRedirect(MollieType $mollieType, DefaultType $defaultType) {
        $paymentMethod = $this->reservationManager->get()->getPayment()->getPaymentMethod()->getType();

        if ($paymentMethod === SettingEnum::VALUES_PAYMENT_METHOD_MOLLIE) {
            $typeHandler = $mollieType;
        } else {
            $typeHandler = $defaultType;
        }

        $reservation = $this->reservationManager->get();

        return $typeHandler->afterPayment($reservation);
    }

    public function webhook(Request $request, MollieType $mollieType)
    {
        $id = $request->request->get("id");

        if (!$id) {
            return new Response("No ID Given", 500);
        }

        /** @var Payment $payment */
        $payment = $this->paymentService->getRepository()->findOneByExternalId($id);

        /** @var MollieType $typeHandler */
        $typeHandler = $mollieType;

        if ($typeHandler->isRefunded($payment)) {
            $this->paymentService->refund($payment);
            return new Response("Payment has been refunded", Response::HTTP_OK);
        }

        if (!$typeHandler->checkPayment($payment)) {
            return new Response("Status has not changed", Response::HTTP_OK);
        }

        $this->paymentService->pay($payment);

        $typeHandler->afterSuccessWebhook($payment);

        return new Response("Status has been updated");
    }
}
