<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Controller\Admin;

use Comsa\SuluReservations\Entity\Reservation;
use Comsa\SuluReservations\Service\ReservationService;
use Sulu\Component\Rest\AbstractRestController;
use HandcraftedInTheAlps\RestRoutingBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\View\ViewHandlerInterface;
use Sulu\Component\Rest\ListBuilder\Doctrine\DoctrineListBuilder;
use Sulu\Component\Rest\ListBuilder\Doctrine\DoctrineListBuilderFactoryInterface;
use Sulu\Component\Rest\ListBuilder\ListRepresentation;
use Sulu\Component\Rest\ListBuilder\Metadata\FieldDescriptorFactoryInterface;
use Sulu\Component\Rest\RestHelperInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Configures the REST Service for Reservations
 * @package Comsa\SuluReservations\Controller\Admin
 */
class ReservationController extends AbstractRestController implements ClassResourceInterface
{
    private ViewHandlerInterface $viewHandler;
    private DoctrineListBuilderFactoryInterface $doctrineListBuilder;
    private FieldDescriptorFactoryInterface $fieldDescriptorFactory;
    private RestHelperInterface $restHelper;
    private ReservationService $service;

    public function __construct(
        ViewHandlerInterface $viewHandler,
        DoctrineListBuilderFactoryInterface $doctrineListBuilderFactory,
        FieldDescriptorFactoryInterface $fieldDescriptorFactory,
        RestHelperInterface $restHelper,
        ReservationService $service
    )
    {
        $this->viewHandler = $viewHandler;
        $this->viewHandler = $viewHandler;
        $this->doctrineListBuilderFactory = $doctrineListBuilderFactory;
        $this->fieldDescriptorFactory = $fieldDescriptorFactory;
        $this->restHelper = $restHelper;
        $this->service = $service;

        parent::__construct($viewHandler);
    }

    public function cgetAction(Request $request): Response
    {
        $fieldDescriptors = $this->fieldDescriptorFactory->getFieldDescriptors(Reservation::RESOURCE_KEY);
        $listBuilder = $this->doctrineListBuilderFactory->create(Reservation::class);

        if ($request->get("id")) {
            $id = $request->get("id");
            if (isset($fieldDescriptors["reservableId"])) {
                $listBuilder->where($fieldDescriptors["reservableId"], $id);
            }
        }


        $this->restHelper->initializeListBuilder($listBuilder, $fieldDescriptors);
        $list = $listBuilder->execute();

        foreach ($list as &$item) {
            /** @var Reservation $reservation */
            $reservation = $this->service->getRepository()->find($item["id"]);
            $item['price'] = '€' . number_format($reservation->getPrice(), 2);
            $item['firstName'] = $reservation->getFormdata()->getFirstName();
            $item['lastName'] = $reservation->getFormdata()->getLastName();
            $item['email'] = $reservation->getFormdata()->getEmail();
            $item['phone'] = $reservation->getFormdata()->getPhone();
        }

        $representation = new ListRepresentation(
          $list,
          "comsa_re_reservations",
          $request->get("_route"),
          $request->query->all(),
          $listBuilder->getCurrentPage(),
          $listBuilder->getLimit(),
          $listBuilder->count()
        );

        return $this->handleView($this->view($representation));
    }

    public function getAction(int $id): Response {
        $reservation = $this->service->getRepository()->find($id);

        if (!$reservation) {
            throw new NotFoundHttpException();
        }

        return $this->handleView($this->view($reservation));
    }

    public function deleteAction(int $id): Response {
        $reservation = $this->service->getRepository()->find($id);

        if (!$reservation) {
            throw new NotFoundHttpException();
        }

        $this->service->delete($reservation);

        return $this->handleView($this->view());
    }

    public function putPaidAction(int $id): Response
    {
        /** @var Reservation $reservation */
        $reservation = $this->service->getRepository()->find($id);
        $this->service->setPaid($reservation);
        
        return $this->handleView($reservation);
    }
}
