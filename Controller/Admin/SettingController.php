<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Controller\Admin;

use Comsa\SuluReservations\Entity\Payment;
use Comsa\SuluReservations\Entity\PaymentMethod;
use Comsa\SuluReservations\Entity\Setting;
use Comsa\SuluReservations\Enum\SettingEnum;
use Comsa\SuluReservations\Factory\PaymentMethodFactory;
use Comsa\SuluReservations\Repository\SettingRepository;
use Comsa\SuluReservations\Service\PaymentMethodService;
use Comsa\SuluReservations\Service\SettingService;
use Comsa\SuluReservations\Utility\TypeConverter;
use Doctrine\Common\Collections\ArrayCollection;
use Sulu\Component\Rest\AbstractRestController;
use HandcraftedInTheAlps\RestRoutingBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\View\ViewHandlerInterface;
use Sulu\Component\Rest\ListBuilder\Doctrine\DoctrineListBuilderFactoryInterface;
use Sulu\Component\Rest\ListBuilder\ListRepresentation;
use Sulu\Component\Rest\ListBuilder\Metadata\FieldDescriptorFactoryInterface;
use Sulu\Component\Rest\RestHelperInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Configures the REST Service for Settings
 * @package Comsa\SuluReservations\Controller\Admin
 */
class SettingController extends AbstractRestController implements ClassResourceInterface
{
    private ViewHandlerInterface $viewHandler;
    private DoctrineListBuilderFactoryInterface $doctrineListBuilder;
    private FieldDescriptorFactoryInterface $fieldDescriptorFactory;
    private RestHelperInterface $restHelper;
    private SettingService $service;
    private PaymentMethodService $paymentMethodService;

    public function __construct(
        ViewHandlerInterface $viewHandler,
        DoctrineListBuilderFactoryInterface $doctrineListBuilderFactory,
        FieldDescriptorFactoryInterface $fieldDescriptorFactory,
        RestHelperInterface $restHelper,
        SettingService $service,
        PaymentMethodService $paymentMethodService,
    )
    {
        $this->viewHandler = $viewHandler;
        $this->viewHandler = $viewHandler;
        $this->doctrineListBuilderFactory = $doctrineListBuilderFactory;
        $this->fieldDescriptorFactory = $fieldDescriptorFactory;
        $this->restHelper = $restHelper;
        $this->service = $service;
        $this->paymentMethodService = $paymentMethodService;

        parent::__construct($viewHandler);
    }

    public function cgetAction(Request $request): Response
    {
        $listBuilder = $this->doctrineListBuilderFactory->create(Setting::class);
        $fieldDescriptors = $this->fieldDescriptorFactory->getFieldDescriptors(Setting::RESOURCE_KEY);
        $this->restHelper->initializeListBuilder($listBuilder, $fieldDescriptors);

        $representation = new ListRepresentation(
            $listBuilder->execute(),
            "comsa_re_settings",
            $request->get("_route"),
            $request->query->all(),
            $listBuilder->getCurrentPage(),
            $listBuilder->getLimit(),
            $listBuilder->count()
        );

        return $this->handleView($this->view($representation));
    }

    public function getAction(int $id, Request $request): Response
    {
        /** @var Setting $setting */
        $setting = $this->service->getRepository()->find($id);

        if (!$setting) {
            throw new NotFoundHttpException();
        }

        $formValueName = match ($setting->getKey()) {
            SettingEnum::KEYS_PAYMENT_METHOD => SettingEnum::FORM_KEY_PAYMENT_METHOD,
            SettingEnum::KEYS_RESERVATION_COMPLETED_TEXT => SettingEnum::FORM_KEY_RESERVATION_COMPLETED_TEXT,
            SettingEnum::KEYS_IBAN => SettingEnum::FORM_KEY_DEFAULT,
            SettingEnum::KEYS_BIC => SettingEnum::FORM_KEY_DEFAULT,
            SettingEnum::KEYS_EMAIL => SettingEnum::FORM_KEY_EMAIL
        };

        if ($setting->getKey() === SettingEnum::KEYS_PAYMENT_METHOD) {
            $paymentMethods = $this->paymentMethodService->getRepository()->findAll();
            if (!empty($paymentMethods)) {
                /** @var PaymentMethod $paymentMethod*/
                foreach ($paymentMethods as $paymentMethod) {
                    switch ($paymentMethod->getType()) {
                        case SettingEnum::VALUES_PAYMENT_METHOD_MOLLIE:
                            $titleMollie = $paymentMethod->getName();
                            $priceMollie = $paymentMethod->getPrice();
                            break;
                        case SettingEnum::VALUES_PAYMENT_METHOD_BANK:
                            $titleBank = $paymentMethod->getName();
                            $priceBank = $paymentMethod->getPrice();
                            break;
                        case SettingEnum::VALUES_PAYMENT_METHOD_CASH:
                            $titleCash = $paymentMethod->getName();
                            $priceCash = $paymentMethod->getPrice();
                            break;
                    }
                }
            }
        }

        $response = new JsonResponse();
        $response->setData([
            "id" => $setting->getId(),
            "title" => $setting->getTitle(),
            "key" => $setting->getKey(),
            $formValueName => $setting->getValue(),
            SettingEnum::FORM_KEY_NAME_MOLLIE => $titleMollie ?? null,
            SettingEnum::FORM_KEY_PRICE_MOLLIE => $priceMollie ?? null,
            SettingEnum::FORM_KEY_NAME_BANK => $titleBank ?? null,
            SettingEnum::FORM_KEY_PRICE_BANK => $priceBank ?? null,
            SettingEnum::FORM_KEY_NAME_CASH => $titleCash ?? null,
            SettingEnum::FORM_KEY_PRICE_CASH => $priceCash ?? null
        ]);

        return $response;
    }

    public function putAction(int $id, Request $request): Response
    {
        /** @var Setting $setting */
        $setting = $this->service->getRepository()->find($id);

        if (!$setting) {
            throw new NotFoundHttpException();
        }

        switch ($setting->getKey()) {
            case SettingEnum::KEYS_PAYMENT_METHOD:
                if (is_array($request->request->get(SettingEnum::FORM_KEY_PAYMENT_METHOD))) {
                    $value = implode(" ,", $request->request->get(SettingEnum::FORM_KEY_PAYMENT_METHOD));
                } else {
                    $value = $request->request->get(SettingEnum::FORM_KEY_PAYMENT_METHOD);
                }

                $this->checkPaymentMethods($request);
                break;
            case SettingEnum::KEYS_RESERVATION_COMPLETED_TEXT:
                $value = $request->request->get(SettingEnum::FORM_KEY_RESERVATION_COMPLETED_TEXT);
                break;
            case SettingEnum::KEYS_EMAIL:
                $value = $request->request->get(SettingEnum::FORM_KEY_EMAIL);
                break;
            default:
                $value = $request->request->get(SettingEnum::FORM_KEY_DEFAULT);
        }

        $this->service->update($setting, [
            "value" => $value
        ]);

        return $this->handleView($this->view($setting));
    }

    protected function checkPaymentMethods(Request $request)
    {
        $oldPaymentMethods = $this->paymentMethodService->getRepository()->findAll();
        $oldPaymentMethodKeys = [];
        foreach ($oldPaymentMethods as $oldPaymentMethod) {
            $oldPaymentMethodKeys[] = $oldPaymentMethod->getType();
        }


        if (is_array($request->request->get(SettingEnum::FORM_KEY_PAYMENT_METHOD))) {
            $newPaymentMethods = $request->request->get(SettingEnum::FORM_KEY_PAYMENT_METHOD);
        } else {
            if ($request->request->get(SettingEnum::FORM_KEY_PAYMENT_METHOD) !== null) {
                $newPaymentMethods = explode(" ,", $request->request->get(SettingEnum::FORM_KEY_PAYMENT_METHOD));
            } else {
                $newPaymentMethods = [];
            }
        }

        $newMethodEntities = new ArrayCollection();

        foreach ($newPaymentMethods as $paymentMethod) {
            $name = match ($paymentMethod) {
              SettingEnum::VALUES_PAYMENT_METHOD_MOLLIE => $request->request->get(SettingEnum::FORM_KEY_NAME_MOLLIE),
              SettingEnum::VALUES_PAYMENT_METHOD_BANK => $request->request->get(SettingEnum::FORM_KEY_NAME_BANK),
              SettingEnum::VALUES_PAYMENT_METHOD_CASH => $request->request->get(SettingEnum::FORM_KEY_NAME_CASH)
            };

            $price = match ($paymentMethod) {
                SettingEnum::VALUES_PAYMENT_METHOD_MOLLIE => $request->request->get(SettingEnum::FORM_KEY_PRICE_MOLLIE),
                SettingEnum::VALUES_PAYMENT_METHOD_BANK => $request->request->get(SettingEnum::FORM_KEY_PRICE_BANK),
                SettingEnum::VALUES_PAYMENT_METHOD_CASH => $request->request->get(SettingEnum::FORM_KEY_PRICE_CASH)
            };

            if (!in_array($paymentMethod, $oldPaymentMethodKeys)) {
                $newMethodEntities->add($this->addPaymentMethod($name, $paymentMethod, TypeConverter::stringToFloat($price)));
            } else {
                if (gettype($price) === "integer") {
                    $price = TypeConverter::intToFloat($price);
                }

                if (gettype($price) === "string") {
                    $price = TypeConverter::stringToFloat($price);
                }

                $newMethodEntities->add(
                    $this->updatePaymentMethod(
                        $this->paymentMethodService->getRepository()->findOneByType($paymentMethod),
                        $name,
                        $price
                    )
                );
            }
        }

        foreach ($oldPaymentMethods as $paymentMethod) {
            if (!$newMethodEntities->contains($paymentMethod)) {
                $this->deletePaymentMethod($paymentMethod);
            }
        }
    }

    private function addPaymentMethod(string $name, string $type, float $price): PaymentMethod {
        return $this->paymentMethodService->create([
            "name" => $name,
            "type" => $type,
            "price" => $price
        ]);
    }

    private function updatePaymentMethod(PaymentMethod $paymentMethod, string $name, float $price): PaymentMethod {
        $this->paymentMethodService->update($paymentMethod, [
            "name" => $name,
            "price" => $price
        ]);

        return $paymentMethod;
    }

    private function deletePaymentMethod(PaymentMethod $paymentMethod): void {
        $this->paymentMethodService->delete($paymentMethod);
    }
}
