<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Controller\Admin;

use Comsa\SuluReservations\Entity\GroupPrice;
use Comsa\SuluReservations\Entity\Option;
use Comsa\SuluReservations\Entity\Reservable;
use Comsa\SuluReservations\Entity\ReservableGroupPrice;
use Comsa\SuluReservations\Entity\ReservableOption;
use Comsa\SuluReservations\Entity\TaxCategory;
use Comsa\SuluReservations\Enum\ReservableEnum;
use Comsa\SuluReservations\Event\Reservable\ReservableCreatedEvent;
use Comsa\SuluReservations\Event\Reservable\ReservableDeletedEvent;
use Comsa\SuluReservations\Event\Reservable\ReservableUpdatedEvent;
use Comsa\SuluReservations\Service\ReservableService;
use Comsa\SuluReservations\Utility\TypeConverter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Sulu\Bundle\FormBundle\Entity\Form;
use Sulu\Bundle\PageBundle\Document\PageDocument;
use Sulu\Component\Content\Types\ResourceLocator\Strategy\ResourceLocatorStrategyPool;
use Sulu\Component\DocumentManager\DocumentManagerInterface;
use Sulu\Component\DocumentManager\Exception\DocumentManagerException;
use Sulu\Component\DocumentManager\Exception\DocumentNotFoundException;
use Sulu\Component\Rest\AbstractRestController;
use HandcraftedInTheAlps\RestRoutingBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\View\ViewHandlerInterface;
use Sulu\Component\Rest\ListBuilder\Doctrine\DoctrineListBuilder;
use Sulu\Component\Rest\ListBuilder\Doctrine\DoctrineListBuilderFactoryInterface;
use Sulu\Component\Rest\ListBuilder\ListRepresentation;
use Sulu\Component\Rest\ListBuilder\Metadata\FieldDescriptorFactoryInterface;
use Sulu\Component\Rest\RestHelperInterface;
use Sulu\Component\Webspace\Manager\WebspaceManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Configures the REST Service for Reservables
 * @package Comsa\SuluReservations\Controller\Admin
 */
class ReservableController extends AbstractRestController implements ClassResourceInterface
{
    private ViewHandlerInterface $viewHandler;
    private DoctrineListBuilderFactoryInterface $doctrineListBuilder;
    private FieldDescriptorFactoryInterface $fieldDescriptorFactory;
    private RestHelperInterface $restHelper;
    private WebspaceManagerInterface $webspaceManager;
    private ReservableService $service;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        ViewHandlerInterface $viewHandler,
        DoctrineListBuilderFactoryInterface $doctrineListBuilderFactory,
        FieldDescriptorFactoryInterface $fieldDescriptorFactory,
        RestHelperInterface $restHelper,
        WebspaceManagerInterface $webspaceManager,
        ReservableService $service,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->viewHandler = $viewHandler;
        $this->viewHandler = $viewHandler;
        $this->doctrineListBuilderFactory = $doctrineListBuilderFactory;
        $this->fieldDescriptorFactory = $fieldDescriptorFactory;
        $this->restHelper = $restHelper;
        $this->webspaceManager = $webspaceManager;
        $this->service = $service;
        $this->eventDispatcher = $eventDispatcher;

        parent::__construct($viewHandler);
    }

    public function cgetAction(Request $request): Response
    {
        $listBuilder = $this->doctrineListBuilderFactory->create(Reservable::class);
        $fieldDescriptors = $this->fieldDescriptorFactory->getFieldDescriptors(Reservable::RESOURCE_KEY);
        $this->restHelper->initializeListBuilder($listBuilder, $fieldDescriptors);
        $listBuilder->where($fieldDescriptors["hidden"], false);

        $representation = new ListRepresentation(
            $listBuilder->execute(),
            "comsa_re_reservables",
            $request->get("_route"),
            $request->query->all(),
            $listBuilder->getCurrentPage(),
            $listBuilder->getLimit(),
            $listBuilder->count()
        );

        return $this->handleView($this->view($representation));
    }

    public function getAction(int $id, Request $request): Response
    {
        /** @var Reservable $reservable */
        $reservable = $this->service->getRepository()->find($id);

        if (!$reservable) {
            throw new NotFoundHttpException();
        }

        return $this->handleView($this->view($reservable->toArray()));
    }

    public function postAction(Request $request)
    {
        $webspaces = $this->webspaceManager->getWebspaceCollection();
        $webspaceKey = $webspaces->toArray()["webspaces"][0]["key"];

        $reservable = $this->createReservable($request);
        $event = new ReservableCreatedEvent($reservable, $request->getLocale(), $webspaceKey);
        $this->eventDispatcher->dispatch($event, ReservableCreatedEvent::NAME);

        return $this->handleView($this->view($reservable));
    }

    public function putAction(int $id, Request $request): Response
    {
        $webspaces = $this->webspaceManager->getWebspaceCollection();
        $webspaceKey = $webspaces->toArray()["webspaces"][0]["key"];

        /** @var Reservable $reservable */
        $reservable = $this->service->getRepository()->find($id);

        if (!$reservable) {
            throw new NotFoundHttpException();
        }

        $reservable = $this->updateReservable($reservable, $request);
        $event = new ReservableUpdatedEvent($reservable, $request->getLocale(), $webspaceKey);
        $this->eventDispatcher->dispatch($event, ReservableUpdatedEvent::NAME);


        return $this->handleView($this->view($reservable));
    }

    public function deleteAction(int $id, Request $request): Response
    {
        $webspaces = $this->webspaceManager->getWebspaceCollection();
        $webspaceKey = $webspaces->toArray()["webspaces"][0]["key"];

        $locale = $this->getLocale($request);

        /** @var Reservable $reservable */
        $reservable = $this->service->getRepository()->find($id);
        $this->service->delete($reservable);

        $event = new ReservableDeletedEvent($reservable);
        $this->eventDispatcher->dispatch($event, ReservableDeletedEvent::NAME);

        return $this->handleView($this->view());
    }

    private function createReservable(Request $request): Reservable {
        return $this->service->create($request->request->all());
    }

    private function updateReservable(Reservable $reservable, Request $request): Reservable {
        $this->service->update($reservable, $request->request->all());

        return $reservable;
    }
}
