<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Event\Reservable;

use Comsa\SuluReservations\Entity\Reservable;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * @package Comsa\SuluReservations\Event\Reservable
 */
class ReservableCreatedEvent extends Event {
    const NAME = "comsa.re.reservable.created";

    private Reservable $reservable;
    private string $locale;
    private string $webspaceKey;

    public function __construct(Reservable $reservable, string $locale, string $webspaceKey) {
        $this->reservable = $reservable;
        $this->locale = $locale;
        $this->webspaceKey = $webspaceKey;
    }

    public function getReservable(): Reservable {
        return $this->reservable;
    }

    public function getLocale(): string {
        return $this->locale;
    }

    public function getWebspaceKey(): string {
        return $this->webspaceKey;
    }
}
