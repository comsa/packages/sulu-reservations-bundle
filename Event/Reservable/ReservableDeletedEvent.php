<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Event\Reservable;

use Comsa\SuluReservations\Entity\Reservable;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * @package Comsa\SuluReservations\Event\Reservable
 */
class ReservableDeletedEvent extends Event {
    const NAME = "comsa.re.reservable.deleted";

    private Reservable $reservable;

    public function __construct(Reservable $reservable) {
        $this->reservable = $reservable;
    }

    public function getReservable(): Reservable {
        return $this->reservable;
    }
}
