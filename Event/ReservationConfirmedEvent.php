<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Event;

use Cassandra\Custom;
use Comsa\SuluReservations\Entity\Reservation;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * @package Comsa\SuluReservations\Event
 */
class ReservationConfirmedEvent extends Event
{
    CONST NAME = 'comsa.re.reservation.confirmed';

    private Reservation $reservation;

    public function __construct(Reservation $reservation)
    {
        $this->reservation = $reservation;
    }

    public function getReservation(): Reservation
    {
        return $this->reservation;
    }

}
