<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\DataFixtures;

use Comsa\SuluReservations\Entity\Setting;
use Comsa\SuluReservations\Enum\SettingEnum;
use Comsa\SuluReservations\Factory\SettingFactory;
use Comsa\SuluReservations\Repository\SettingRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * AppSeed for the Reservations Module
 * @package Comsa\SuluReservations\DataFixtures
 */
class AppSeed extends Fixture
{
    private SettingRepository $settingRepository;

    public function __construct(SettingRepository $settingRepository) {
        $this->settingRepository = $settingRepository;
    }

    public function load(ObjectManager $manager)
    {
        if (!$this->settingRepository->findOneByKey(SettingEnum::KEYS_PAYMENT_METHOD)) {
            $manager->persist($this->loadSettingPaymentMethod());
        }

        if (!$this->settingRepository->findOneByKey(SettingEnum::KEYS_IBAN)) {
            $manager->persist($this->loadSettingIBAN());
        }

        if (!$this->settingRepository->findOneByKey(SettingEnum::KEYS_BIC)) {
            $manager->persist($this->loadSettingBIC());
        }

        if (!$this->settingRepository->findOneByKey(SettingEnum::KEYS_EMAIL)) {
            $manager->persist($this->loadSettingEmail());
        }

        if (!$this->settingRepository->findOneByKey(SettingEnum::KEYS_RESERVATION_COMPLETED_TEXT)) {
            $manager->persist($this->loadSettingReservationCompletedText());
        }

        $manager->flush();
    }

    public function loadSettingPaymentMethod(): Setting
    {
        return SettingFactory::create(
            title: SettingEnum::TITLES_PAYMENT_METHOD,
            key: SettingEnum::KEYS_PAYMENT_METHOD,
            value: null
        );
    }

    public function loadSettingIBAN(): Setting
    {
        return SettingFactory::create(
          title: SettingEnum::TITLES_IBAN,
          key: SettingEnum::KEYS_IBAN,
          value: null
        );
    }

    public function loadSettingBIC(): Setting
    {
        return SettingFactory::create(
          title: SettingEnum::TITLES_BIC,
          key: SettingEnum::KEYS_BIC,
          value: null
        );
    }

    public function loadSettingEmail(): Setting
    {
        return SettingFactory::create(
          title: SettingEnum::TITLES_EMAIL,
          key: SettingEnum::KEYS_EMAIL,
          value: null
        );
    }

    public function loadSettingReservationCompletedText(): Setting
    {
        return SettingFactory::create(
          title: SettingEnum::TITLES_RESERVATION_COMPLETED_TEXT,
          key: SettingEnum::KEYS_RESERVATION_COMPLETED_TEXT,
          value: null
        );
    }
}
