<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Factory;

use Comsa\SuluReservations\Entity\Reservable;
use Comsa\SuluReservations\Entity\TaxCategory;
use Doctrine\Common\Collections\Collection;
use Sulu\Bundle\FormBundle\Entity\Form;

/**
 * Handles the creation of Reservables
 * @package Comsa\SuluReservations\Factory
 */
class ReservableFactory
{
    public static function create(
        string $title,
        string $type,
        string $priceType,
        ?float $singlePrice,
        bool $singleReservation,
        int $limit,
        \DateTime $start,
        \DateTime $end,
        Form $form,
        Collection $groupPrices,
        Collection $options,
        string $parentPage,
        string $optionSelectionType,
        ?TaxCategory $taxCategory
    ): Reservable {
        return (new Reservable())
            ->setTitle($title)
            ->setType($type)
            ->setPriceType($priceType)
            ->setSinglePrice($singlePrice)
            ->setSingleReservation($singleReservation)
            ->setLimit($limit)
            ->setStart($start)
            ->setEnd($end)
            ->setForm($form)
            ->setGroupPrices($groupPrices)
            ->setOptions($options)
            ->setParentPage($parentPage)
            ->setOptionSelectionType($optionSelectionType)
            ->setTaxCategory($taxCategory)
        ;
    }
}
