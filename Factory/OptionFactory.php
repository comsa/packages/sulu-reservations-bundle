<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Factory;

use Comsa\SuluReservations\Entity\Option;

/**
 * Handles the creation of Options
 * @package Comsa\SuluReservations\Factory
 */
class OptionFactory
{
    public static function create(
        string $title,
        float $price
    ): Option {
        return (new Option())
            ->setTitle($title)
            ->setPrice($price)
        ;
    }
}
