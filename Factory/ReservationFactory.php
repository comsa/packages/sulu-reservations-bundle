<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Factory;

use Comsa\SuluReservations\Entity\Reservable;
use Comsa\SuluReservations\Entity\Reservation;
use Sulu\Bundle\FormBundle\Entity\Dynamic;

/**
 * Handles the creation of reservations
 * @package Comsa\SuluReservations\factory
 */
class ReservationFactory
{
    public static function create(
        Reservable $reservable,
        int $amount,
        Dynamic $suluDynamics,
    ): Reservation {
        return (new Reservation())
            ->setReservable($reservable)
            ->setAmount($amount)
            ->setFormdata($suluDynamics)
            ->setPayment(null)
        ;
    }
}
