<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Factory;

use Comsa\SuluReservations\Entity\TaxCategory;

class TaxCategoryFactory
{
    public static function create(
        string $title,
        float $percentage
    ): TaxCategory
    {
        return (new TaxCategory())
            ->setTitle($title)
            ->setPercentage($percentage)
        ;
    }
}
