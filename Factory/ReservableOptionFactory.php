<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Factory;

use Comsa\SuluReservations\Entity\Option;
use Comsa\SuluReservations\Entity\Reservable;
use Comsa\SuluReservations\Entity\ReservableOption;

/**
 * Handles the creation of Reservable Options
 * @package Comsa\SuluReservations\Factory
 */
class ReservableOptionFactory
{
    public static function create(
        ?Reservable $reservable,
        Option $option,
        int $position
    ): ReservableOption {
        return (new ReservableOption())
            ->setReservable($reservable)
            ->setOption($option)
            ->setPosition($position)
        ;
    }
}
