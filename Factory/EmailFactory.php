<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Factory;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;

/**
 * Handles the creation of Emails
 */
class EmailFactory
{
    public static function create(
        string $from,
        array $to,
        string $subject,
        array $cc,
        string $bcc,
        string $template,
        array $params
    ): TemplatedEmail {
        return (new TemplatedEmail())
            ->from($from)
            ->to(...$to)
            ->cc(...$cc)
            ->bcc($bcc)
            ->subject($subject)
            ->htmlTemplate($template)
            ->context($params)
        ;
    }
}
