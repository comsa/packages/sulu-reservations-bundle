<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Factory;

use Comsa\SuluReservations\Entity\Group;
use Comsa\SuluReservations\Entity\GroupPrice;
use Comsa\SuluReservations\Entity\Reservation;

/**
 * Handles the creation of Groups
 */
class GroupFactory
{
    public static function create(
        Reservation $reservation,
        GroupPrice $group,
        int $amount,
        float $price
    ): Group {
        return (new Group())
            ->setReservation($reservation)
            ->setGroup($group)
            ->setAmount($amount)
            ->setPrice($price)
        ;
    }
}
