<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Factory;

use Comsa\SuluReservations\Entity\Setting;

/**
 * Handles creation of Setting
 * @package Comsa\SuluReservations\Factory
 */
class SettingFactory {
    public static function create(
        string $title,
        string $key,
        ?string $value
    ): Setting {
        return (new Setting())
            ->setTitle($title)
            ->setKey($key)
            ->setValue($value)
        ;
    }
}
