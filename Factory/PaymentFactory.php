<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Factory;

use Comsa\SuluReservations\Entity\Payment;
use Comsa\SuluReservations\Entity\PaymentMethod;

/**
 * Handles the creation of Payments
 * @package Comsa\SuluReservations\Factory
 */
class PaymentFactory
{
    public static function create(
        PaymentMethod $paymentMethod,
        float $amount
    ): Payment {
        return (new Payment())
            ->setPaymentMethod($paymentMethod)
            ->setAmount($amount)
        ;
    }
}
