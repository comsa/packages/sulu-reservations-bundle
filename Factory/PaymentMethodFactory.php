<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Factory;

use Comsa\SuluReservations\Entity\PaymentMethod;

/**
 * Handles the creation of Payment Methods
 * @package Comsa\SuluReservations\Factory
 */
class PaymentMethodFactory
{
    public static function create(
        string $name,
        string $type,
        float $price
    ): PaymentMethod
    {
        return (new PaymentMethod())
            ->setName($name)
            ->setType($type)
            ->setPrice($price)
        ;
    }
}
