<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Factory;

use Comsa\SuluReservations\Entity\GroupPrice;

/**
 * Handles creation of Group Prices
 * @package Comsa\SuluReservations\Factory
 */
class GroupPriceFactory
{
    public static function create(
        string $groupName,
        float $groupPrice,
    ): GroupPrice {
        return (new GroupPrice())
            ->setGroupName($groupName)
            ->setPrice($groupPrice)
        ;
    }
}
