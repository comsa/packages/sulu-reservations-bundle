<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Factory;

use Comsa\SuluReservations\Entity\GroupPrice;
use Comsa\SuluReservations\Entity\Reservable;
use Comsa\SuluReservations\Entity\ReservableGroupPrice;

/**
 * Handles creation of Reservable Group Prices
 * @package Comsa\SuluReservations\Factory
 */
class ReservableGroupPriceFactory
{
    public static function create(
        ?Reservable $reservable,
        GroupPrice $groupPrice,
        int $position
    ): ReservableGroupPrice {
        return (new ReservableGroupPrice())
            ->setReservable($reservable)
            ->setGroupPrice($groupPrice)
            ->setPosition($position)
        ;
    }
}
