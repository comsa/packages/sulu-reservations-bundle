<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Enum;

/**
 * Defines all constants for Reservable
 * @package Comsa\SuluReservations\Enum
 */
class ReservableEnum
{
    public const TYPE_ACTIVITY = "ACTIVITY";
    public const TYPE_VISIT = "VISIT";

    public const PRICE_TYPE_SINGLE = "SINGLE_PAYMENT";
    public const PRICE_TYPE_PP = "PER_PERSON";
}
