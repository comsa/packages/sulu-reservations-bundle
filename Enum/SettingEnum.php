<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Enum;

/**
 * Defines all constants for Setting
 * @package Comsa\SuluReservations\Enum
 */
class SettingEnum {
    public const TITLES_PAYMENT_METHOD = "Payment Method";
    public const TITLES_IBAN = "IBAN";
    public const TITLES_BIC = "BIC";
    public const TITLES_EMAIL = "Email";
    public const TITLES_RESERVATION_COMPLETED_TEXT = "Text after Reservation";

    public const KEYS_PAYMENT_METHOD = "paymentMethod";
    public const KEYS_IBAN = "IBAN";
    public const KEYS_BIC = "BIC";
    public const KEYS_EMAIL = "email";
    public const KEYS_RESERVATION_COMPLETED_TEXT = "reservationCompletedText";

    public const VALUES_PAYMENT_METHOD_MOLLIE = "mollie";
    public const VALUES_PAYMENT_METHOD_BANK = "bank";
    public const VALUES_PAYMENT_METHOD_CASH = "cash";

    public const FORM_KEY_PAYMENT_METHOD = "valuePaymentMethod";
    public const FORM_KEY_RESERVATION_COMPLETED_TEXT = "valueReservationCompletedText";
    public const FORM_KEY_EMAIL = "valueEmail";

    public const FORM_KEY_DEFAULT = "value";
    public const FORM_KEY_NAME_MOLLIE = "nameMollie";
    public const FORM_KEY_NAME_BANK = "nameBank";
    public const FORM_KEY_NAME_CASH = "nameCash";

    public const FORM_KEY_PRICE_MOLLIE = "priceMollie";
    public const FORM_KEY_PRICE_BANK = "priceBank";
    public const FORM_KEY_PRICE_CASH = "priceCash";
}
