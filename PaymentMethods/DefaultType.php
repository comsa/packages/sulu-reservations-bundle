<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\PaymentMethods;

use Comsa\SuluReservations\Entity\Payment;
use Comsa\SuluReservations\Entity\Reservation;
use Comsa\SuluReservations\Entity\Setting;
use Comsa\SuluReservations\Enum\SettingEnum;
use Comsa\SuluReservations\Event\ReservationConfirmedEvent;
use Comsa\SuluReservations\PaymentMethods\Interfaces\PaymentMethodInterface;
use Comsa\SuluReservations\Repository\SettingRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Sulu\Bundle\WebsiteBundle\Resolver\TemplateAttributeResolverInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;

/**
 * Default Types that are not Mollie
 * @package Comsa\SuluReservations\PaymentMethods
 */
class DefaultType implements PaymentMethodInterface
{
    private Environment $twig;
    private TemplateAttributeResolverInterface $templateAttributeResolver;
    private ParameterBagInterface $parameterBag;
    private EventDispatcherInterface $eventDispatcher;
    private RouterInterface $router;
    private EntityManagerInterface $entityManager;
    private SettingRepository $settingRepository;

    public function __construct(
        Environment $twig,
        TemplateAttributeResolverInterface $templateAttributeResolver,
        ParameterBagInterface $parameterBag,
        EventDispatcherInterface $eventDispatcher,
        RouterInterface $router,
        EntityManagerInterface $entityManager,
        SettingRepository $settingRepository,
    ) {
        $this->twig = $twig;
        $this->templateAttributeResolver = $templateAttributeResolver;
        $this->parameterBag = $parameterBag;
        $this->eventDispatcher = $eventDispatcher;
        $this->router = $router;
        $this->entityManager = $entityManager;
        $this->settingRepository = $settingRepository;
    }

    public function handlePayment(Payment $payment, Reservation $reservation): Response {
        $this->confirmReservation($reservation);
        return new RedirectResponse($this->router->generate("comsa_sulu_reservations_thanks"));
    }

    public function getAdditionalTemplateData(Reservation $reservation): array {
        return [
            "transfer_details" => $reservation->getPayment()->getPaymentMethod()->getType() !== SettingEnum::VALUES_PAYMENT_METHOD_CASH ? $this->getTransferDetails() : null,
            "thankYouText" => $this->settingRepository->findOneByKey(SettingEnum::KEYS_RESERVATION_COMPLETED_TEXT)->getValue()
        ];
    }

    public function getTransferDetails(): array {
        return [
            "iban" => $this->settingRepository->findOneByKey(SettingEnum::KEYS_IBAN),
            "bic" => $this->settingRepository->findOneByKey(SettingEnum::KEYS_BIC)
        ];
    }

    protected function confirmReservation(Reservation $reservation) {
        $event = new ReservationConfirmedEvent($reservation);
        $this->eventDispatcher->dispatch($event, ReservationConfirmedEvent::NAME);
    }

    public function afterPayment(Reservation $reservation): Response {
        return new RedirectResponse($this->router->generate("comsa_sulu_reservations_thanks"));
    }

    public function checkPayment(Payment $payment): bool {
        return false;
    }

    public function afterSuccessWebhook(Payment $payment): void {
        return;
    }
}
