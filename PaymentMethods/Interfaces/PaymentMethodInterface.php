<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\PaymentMethods\Interfaces;

use Comsa\SuluReservations\Entity\Payment;
use Comsa\SuluReservations\Entity\Reservation;
use Symfony\Component\HttpFoundation\Response;

/**
 * @package Comsa\SuluReservations\PaymentMethods\Interfaces
 */
interface PaymentMethodInterface
{
    public function getAdditionalTemplateData(Reservation $order): array;
    public function handlePayment(Payment $payment, Reservation $reservation): Response;
    public function afterPayment(Reservation $order): Response;
    public function checkPayment(Payment $payment): bool;
    public function afterSuccessWebhook(Payment $payment): void;
}
