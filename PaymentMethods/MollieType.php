<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\PaymentMethods;

use Comsa\SuluReservations\Entity\Payment;
use Comsa\SuluReservations\Entity\Reservation;
use Comsa\SuluReservations\Enum\SettingEnum;
use Comsa\SuluReservations\Event\ReservationConfirmedEvent;
use Comsa\SuluReservations\PaymentMethods\Interfaces\PaymentMethodInterface;
use Comsa\SuluReservations\Service\PaymentService;
use Comsa\SuluReservations\Service\ReservationService;
use Comsa\SuluReservations\Service\SettingService;
use Mollie\Api\MollieApiClient;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

/**
 * Handles MolliePayments
 * @package Comsa\SuluReservations\PaymentMethods
 */
class MollieType implements PaymentMethodInterface {
    private ParameterBagInterface $parameterBag;
    private RouterInterface $router;
    private EventDispatcherInterface $eventDispatcher;
    private MollieApiClient $mollie;
    private SettingService $settingService;
    private ReservationService $reservationService;
    private PaymentService $paymentService;

    public function __construct(
        ParameterBagInterface $parameterBag,
        RouterInterface $router,
        EventDispatcherInterface $eventDispatcher,
        SettingService $settingService,
        ReservationService $reservationService,
        PaymentService $paymentService
    ) {
        $this->parameterBag = $parameterBag;
        $this->router = $router;
        $this->eventDispatcher = $eventDispatcher;
        $this->settingService = $settingService;
        $this->reservationService = $reservationService;
        $this->paymentService = $paymentService;

        $this->mollie = new MollieApiClient();
        $this->mollie->setApiKey($this->parameterBag->get("comsa_sulu_reservations_mollie_api_key"));
    }

    public function getAdditionalTemplateData(Reservation $reservation): array {
        return [
            "thankYouText" => $this->settingService->getRepository()->findOneByKey(SettingEnum::KEYS_RESERVATION_COMPLETED_TEXT)->getValue()
        ];
    }

    public function handlePayment(Payment $payment, Reservation $reservation): Response {
        if ($_SERVER["REMOTE_ADDR"] === "127.0.0.1") {
            $webhookUrl = "https://webhook.site/fef192fe-9cb3-4a11-a4a0-a3e448b78f01";
        } else {
            $webhookUrl = $this->router->generate("comsa_sulu_reservations_webhook_payment", [], RouterInterface::ABSOLUTE_URL);
        }

        $molliePayment = $this->mollie->payments->create([
            "amount" => [
                "currency" => "EUR",
                "value" => number_format($payment->getAmount(), 2)
            ],
            "description" => $this->formatDescription($reservation),
            "redirectUrl" => $this->router->generate("comsa_sulu_reservations_handle_payment_redirect", [], RouterInterface::ABSOLUTE_URL),
            "webhookUrl" => $webhookUrl
        ]);

        $this->paymentService->updateExternalId($payment, $molliePayment->id);

        return new RedirectResponse($molliePayment->getCheckoutUrl());
    }

    public function afterPayment(Reservation $reservation): Response {
        return new RedirectResponse($this->router->generate("comsa_sulu_reservations_thanks"));
    }

    public function checkPayment(Payment $payment): bool {
        $molliePayment = $this->mollie->payments->get($payment->getExternalId());
        return $molliePayment->isPaid();
    }

    public function isRefunded(Payment $payment): bool {
        $molliePayment = $this->mollie->payments->get($payment->getExternalId());
        return $molliePayment->hasRefunds();
    }

    public function afterSuccessWebhook(Payment $payment): void {
        $reservation = $this->reservationService->getRepository()->findOneByPayment($payment);
        $this->confirmReservation($reservation);
    }

    protected function confirmReservation(Reservation $reservation) {
        $event = new ReservationConfirmedEvent($reservation);
        $this->eventDispatcher->dispatch($event, ReservationConfirmedEvent::NAME);
    }

    private function formatDescription(Reservation $reservation): string {
        if ($reservation->getReservable()->getTaxCategory()) {
            return sprintf(
              "Reservatie #%d %s",
              $reservation->getId(),
              $reservation->getReservable()->getTaxCategory()->toString()
            );
        } else {
            return sprintf(
                "Reservatie #%d",
                $reservation->getId(),
            );
        }
    }
}
