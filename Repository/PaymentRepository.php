<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Repository;

use Comsa\SuluReservations\Entity\Payment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @package Comsa\SuluReservations\Repository
 */
class PaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Payment::class);
    }

    public function findOneByExternalId(string $externalId): ?Payment {
        return $this->createQueryBuilder("payment")
            ->where("payment.externalId = :externalId")
            ->setParameter("externalId", $externalId)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
