<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Repository;

use Comsa\SuluReservations\Entity\GroupPrice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @package Comsa\SuluReservations\Repository
 */
class GroupPriceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, GroupPrice::class);
    }

    public function findOneByNameAndPrice(string $groupName, float $groupPrice): ?GroupPrice {
        return $this->createQueryBuilder("groupPrice")
            ->where("groupPrice.groupName = :groupName")
            ->andWhere("groupPrice.groupPrice = :groupPrice")
            ->setParameter("groupName", $groupName)
            ->setParameter("groupPrice", $groupPrice)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
