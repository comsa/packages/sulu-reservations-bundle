<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Repository;

use Comsa\SuluReservations\Entity\Option;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @package Comsa\SuluReservations\Repository
 */
class OptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Option::class);
    }

    public function findOneByTitleAndPrice(string $title, float $price): ?Option {
        return $this->createQueryBuilder("option")
            ->where("option.title = :title")
            ->andWhere("option.price = :price")
            ->setParameter("title", $title)
            ->setParameter("price", $price)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
