<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Repository;

use Comsa\SuluReservations\Entity\ReservableGroupPrice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @package Comsa\SuluReservations\Repository
 */
class ReservableGroupPriceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, ReservableGroupPrice::class);
    }
}
