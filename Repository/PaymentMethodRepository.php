<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Repository;

use Comsa\SuluReservations\Entity\PaymentMethod;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @package Comsa\SuluReservations\Repository
 */
class PaymentMethodRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PaymentMethod::class);
    }

    public function findOneByType(string $type): PaymentMethod {
        return $this->createQueryBuilder("paymentMethod")
            ->where("paymentMethod.type = :type")
            ->setParameter("type", $type)
            ->getQuery()
            ->getSingleResult()
        ;
    }
}
