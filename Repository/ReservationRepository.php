<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Repository;

use Comsa\SuluReservations\Entity\Payment;
use Comsa\SuluReservations\Entity\Reservable;
use Comsa\SuluReservations\Entity\Reservation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @package Comsa\SuluReservations\Repository
 */
class ReservationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reservation::class);
    }

    public function findOneByPayment(Payment $payment): ?Reservation {
        return $this->createQueryBuilder("reservation")
            ->where("reservation.payment = :payment")
            ->setParameter("payment", $payment)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @param Reservable $reservable
     * @return Collection|array|Reservation[]
     */
    public function findByReservable(Reservable $reservable): Collection|array {
        return $this->createQueryBuilder("reservation")
            ->innerJoin("reservation.payment", "payment")
            ->where("reservation.reservable = :reservable")
            ->andWhere("payment.paymentCompletedAt is not NULL")
            ->setParameter("reservable", $reservable)
            ->getQuery()
            ->getResult()
        ;
    }
}
