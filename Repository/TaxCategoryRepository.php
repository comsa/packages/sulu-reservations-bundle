<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Repository;

use Comsa\SuluReservations\Entity\TaxCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class TaxCategoryRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, TaxCategory::class);
    }
}
