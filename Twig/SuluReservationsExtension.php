<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Twig;

use Comsa\SuluReservations\Entity\Reservable;
use Comsa\SuluReservations\Entity\Reservation;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\PersistentCollection;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Defines Twig Functions and Filters used in the Reservation Module
 * @package Comsa\SuluReservations\Twig
 */
class SuluReservationsExtension extends AbstractExtension
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction("convert_group_prices_to_array", [$this, "convertGroupPricesToArray"]),
            new TwigFunction("get_spaces_left", [$this, "getSpacesLeft"])
        ];
    }

    public function convertGroupPricesToArray(PersistentCollection $groupPrices): array
    {
        return $groupPrices->toArray();
    }

    public function getSpacesLeft(int $id): int
    {
        /** @var Reservable $reserable */
        $reserable = $this->entityManager->getRepository(Reservable::class)->find($id);

        $reservationRepository = $this->entityManager->getRepository(Reservation::class);

        $reservations = $reservationRepository->findBy([
           "reservable" => $reserable
        ]);

        if ($reservations) {
            if ($reserable->isSingleReservation()) {
                return 0;
            } else {
                $total = $reserable->getLimit();
                /** @var Reservation $reservation */
                foreach ($reservations as $reservation) {
                    if ($reservation && $reservation->getPayment() && $reservation->getPayment()->getPaymentCompletedAt()) {
                        $total -= $reservation->getAmount();
                    }
                }

                if ($total < 0 ) {
                    return  0;
                }

                return $total;
            }
        }

        return $reserable->getLimit();
    }
}
