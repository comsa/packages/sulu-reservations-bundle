<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Configures the Dependency Injection for the Reservations Module
 * @package Comsa\SuluReservations\DependencyInjection
 */
class SuluReservationsExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . "/../Resources/config"));
        $loader->load("services.yaml");

        if ($container->hasExtension("twig")) {
            $container->loadFromExtension("twig", [
                "paths" => [
                    "%kernel.root_dir%/../vendor/comsa/sulu-reservations/Resources/views"
                ]
            ]);
        }
    }

    public function prepend(ContainerBuilder $container)
    {
        if ($container->hasExtension("fos_js_routing")) {
            $container->prependExtensionConfig(
                "fos_js_routing",
                [
                    "routes_to_expose" => [
                        "comsa_sulu_reservations.get_reservations",
                        "comsa_sulu_reservations.get_reservation",
                        "comsa_sulu_reservations.get_reservables",
                        "comsa_sulu_reservations.get_reservable",
                        "comsa_sulu_reservations.get_tax-categories",
                        "comsa_sulu_reservations.get_tax-category",
                        "comsa_sulu_reservations.get_options",
                        "comsa_sulu_reservations.get_option",
                        "comsa_sulu_reservations.get_settings",
                        "comsa_sulu_reservations.get_setting",
                        "comsa_sulu_reservations.put_reservation_paid",
                        "comsa.reservations.export_reservations"
                    ]
                ]
            );
        }

        if ($container->hasExtension("sulu_admin")) {
            $container->prependExtensionConfig(
                "sulu_admin",
                [
                    "lists" => [
                        "directories" => [
                            __DIR__ . "/../Resources/config/lists"
                        ]
                    ],
                    "forms" => [
                        "directories" => [
                            __DIR__ . "/../Resources/config/forms"
                        ]
                    ],
                    "resources" => [
                        "comsa_re_reservations" => [
                            "routes" => [
                                "list" => "comsa_sulu_reservations.get_reservations",
                                "detail" => "comsa_sulu_reservations.get_reservation"
                            ]
                        ],
                        "comsa_re_reservables" => [
                            "routes" => [
                                "list" => "comsa_sulu_reservations.get_reservables",
                                "detail" => "comsa_sulu_reservations.get_reservable"
                            ]
                        ],
                        "comsa_re_tax_categories" => [
                          "routes" => [
                              "list" => "comsa_sulu_reservations.get_tax-categories",
                              "detail" => "comsa_sulu_reservations.get_tax-category"
                          ]
                        ],
                        "comsa_re_options" => [
                            "routes" => [
                                "list" => "comsa_sulu_reservations.get_options",
                                "detail" => "comsa_sulu_reservations.get_option"
                            ]
                        ],
                        "comsa_re_settings" => [
                            "routes" => [
                                "list" => "comsa_sulu_reservations.get_settings",
                                "detail" => "comsa_sulu_reservations.get_setting",
                            ]
                        ]
                    ],
                    "field_type_options" => [
                        "selection" => [
                          "reservations_option_selection" => [
                              "default_type" => "list_overlay",
                              "resource_key" => "comsa_re_options",
                              "types" => [
                                  "list_overlay" => [
                                      "adapter" => "table",
                                      "list_key" => "comsa_re_options",
                                      "display_properties" => [
                                          "title"
                                      ],
                                      "icon" => "su-list-ul",
                                      "label" => "Reservable Options",
                                      "overlay_title" => "Select your Reservable Options"
                                  ]
                              ]
                          ]
                        ],
                        "single_selection" => [
                            "single_reservable_selection" => [
                                "default_type" => "list_overlay",
                                "resource_key" => "comsa_re_reservables",
                                "types" => [
                                    "list_overlay" => [
                                        "adapter" => "table",
                                        "list_key" => "comsa_re_reservables",
                                        "display_properties" => [
                                            "title"
                                        ],
                                        "icon" => "su-list-ul",
                                        "overlay_title" => "Select your Reservable",
                                        "empty_text" => "Select your Reservable"
                                    ]
                                ]
                            ],
                            "comsa.re.single_selection.tax_category" => [
                                "default_type" => "list_overlay",
                                "resource_key" => "comsa_re_tax_categories",
                                "types" => [
                                    "list_overlay" => [
                                        "adapter" => "table",
                                        "list_key" => "comsa_re_tax_categories",
                                        "display_properties" => [
                                            "title",
                                            "percentage"
                                        ],
                                        "icon" => "su-list-ul",
                                        "overlay_title" => "Select Tax Category",
                                        "empty_text" => "Select Tax Category"
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            );
        }
    }
}
