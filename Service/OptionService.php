<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Service;

use Comsa\SuluReservations\Entity\Interfaces\CrudResource;
use Comsa\SuluReservations\Entity\Option;
use Comsa\SuluReservations\Entity\Reservable;
use Comsa\SuluReservations\Entity\ReservableOption;
use Comsa\SuluReservations\Enum\ReservableEnum;
use Comsa\SuluReservations\Factory\OptionFactory;
use Comsa\SuluReservations\Factory\ReservableOptionFactory;
use Comsa\SuluReservations\Repository\OptionRepository;
use Comsa\SuluReservations\Repository\ReservableOptionRepository;
use Comsa\SuluReservations\Service\Interfaces\BaseCrudServiceInterface;
use Comsa\SuluReservations\Utility\TypeConverter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * CRUD Service for the Option class.
 * @package Comsa\SuluReservations\Service
 */
class OptionService extends AbstractCrudService implements BaseCrudServiceInterface {
    private ReservableOptionRepository $reservableOptionRepository;

    public function __construct(EntityManagerInterface $entityManager, OptionRepository $repository, ReservableOptionRepository $reservableOptionRepository) {
        $this->reservableOptionRepository = $reservableOptionRepository;

        parent::__construct($entityManager, $repository, Option::class);
    }

    public function create(array $data): CrudResource {
        $entity = OptionFactory::create(
          $data["optionTitle"],
          $data["optionPrice"]
        );

        $this->save($entity);
        return $entity;
    }

    public function update(CrudResource $entity, array $data): void {
    }

    public function map(?array $data): Collection {
        $optionPosition = 0;
        $options = new ArrayCollection();

        if (is_array($data)) {
            foreach ($data as $option) {
                $optionObject = $this->getRepository()->findOneByTitleAndPrice(
                  $option["optionTitle"],
                  TypeConverter::stringToFloat((string) $option["optionPrice"])
                );

                if (!$optionObject) {
                    $optionObject = $this->create([
                        "optionTitle" => $option["optionTitle"],
                        "optionPrice" => TypeConverter::stringToFloat((string) $option["optionPrice"])
                    ]);
                }

                $options->add(
                    ReservableOptionFactory::create(
                        null,
                        $optionObject,
                        $optionPosition
                    )
                );

                $optionPosition++;
            }
        }

        return $options;
    }

    public function clean(Reservable $reservable, ?array $data): Collection {
        $existingOptions = [];
        $newOptions = new ArrayCollection();
        $newReservableOptions = new ArrayCollection();

        foreach ($reservable->getOptions() as $existingOption) {
            $existingOptions[$existingOption->getId()] = $existingOption->__toString();
        }

        if (is_array($data)) {
            $optionPosition = 0;
            foreach ($data as $option) {
                if (
                    !in_array(
                        sprintf("%s%d", $option["optionTitle"], $option["optionPrice"]),
                        $existingOptions
                    )
                ) {
                    $optionEntity = $this->getRepository()->findOneByTitleAndPrice(
                        $option["optionTitle"],
                        TypeConverter::stringToFloat((string) $option["optionPrice"])
                    );

                    if (!$optionEntity) {
                        $optionEntity = $this->create([
                            "optionTitle" => $option["optionTitle"],
                            "optionPrice" => TypeConverter::stringToFloat((string) $option["optionPrice"])
                        ]);
                    }

                    $reservableOption = ReservableOptionFactory::create(
                      $reservable,
                      $optionEntity,
                      $optionPosition
                    );

                    $newReservableOptions->add($reservableOption);
                    $this->entityManager->persist($reservableOption);
                } else {
                    $id = array_search(
                        sprintf("%s%d", $option["optionTitle"], $option["optionPrice"]),
                        $existingOptions
                    );

                    /** @var ReservableOption $optionToUpdate */
                    $optionToUpdate = $this->reservableOptionRepository->find($id);
                    $optionToUpdate->setPosition($optionPosition);
                    $newReservableOptions->add($optionToUpdate);
                }

                $newOptions->add(sprintf(
                   "%s%d", $option["optionTitle"], $option["optionPrice"]
                ));

                $optionPosition++;
            }
        }

        foreach ($existingOptions as $key => $existingOption) {
            if (!$newOptions->contains($existingOption)) {
                $optionObject = $this->reservableOptionRepository->find($key);
                $reservable->removeOption($optionObject);
                $this->entityManager->remove($optionObject);
            }
        }

        return $newReservableOptions;
    }
}
