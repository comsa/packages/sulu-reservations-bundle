<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Service;

use Comsa\SuluReservations\Entity\Interfaces\CrudResource;
use Comsa\SuluReservations\Entity\Payment;
use Comsa\SuluReservations\Factory\PaymentFactory;
use Comsa\SuluReservations\Repository\PaymentRepository;
use Comsa\SuluReservations\Service\Interfaces\BaseCrudServiceInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class PaymentService extends AbstractCrudService  implements BaseCrudServiceInterface {
    public function __construct(EntityManagerInterface $entityManager, PaymentRepository $repository) {
        parent::__construct($entityManager, $repository, Payment::class);
    }

    public function create(array $data): CrudResource {
        $payment = PaymentFactory::create(
            $data["paymentMethod"],
            $data["amount"]
        );

        $this->save($payment);

        return $payment;
    }

    public function update(CrudResource $entity, array $data): void {
        // TODO: Implement update() method.
    }

    public function updateExternalId(Payment $payment, string $id): void {
        $payment->setExternalId($id);
        $this->save($payment);
    }

    public function refund(Payment $payment): void {
        $payment->refund();
        $this->save($payment);
    }

    public function pay(Payment $payment): void {
        $payment->setPaid();
        $this->save($payment);
    }
}
