<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Service;

use Comsa\SuluReservations\Entity\Reservable;
use Doctrine\ORM\EntityManagerInterface;
use Sulu\Bundle\PageBundle\Document\PageDocument;
use Sulu\Component\Content\Types\ResourceLocator\Strategy\ResourceLocatorStrategyPoolInterface;
use Sulu\Component\DocumentManager\DocumentManagerInterface;
use Sulu\Component\DocumentManager\Exception\DocumentNotFoundException;
use function PHPUnit\Framework\isReadable;

/**
 * Service that provides basic creation and deletion of Reservable pages
 * @package Comsa\SuluReservations\Service
 */
class DocumentService {
    private ReservableService $reservableService;
    private DocumentManagerInterface $documentManager;
    private ResourceLocatorStrategyPoolInterface $resourceLocatorStrategyPool;

    public function __construct(ReservableService $reservableService, DocumentManagerInterface $documentManager, ResourceLocatorStrategyPoolInterface $resourceLocatorStrategyPool) {
        $this->reservableService = $reservableService;
        $this->documentManager = $documentManager;
        $this->resourceLocatorStrategyPool = $resourceLocatorStrategyPool;
    }

    public function create(Reservable $reservable, string $locale, string $webspaceKey) {
        /** @var PageDocument $parent */
        $parent = $this->documentManager->find($reservable->getParentPage());

        if ($reservable->getPageId()) {
            $this->delete($reservable);
        }

        /** @var PageDocument $reservablePage */
        $reservablePage = $this->documentManager->create("page");
        $reservablePage->setTitle($reservable->getTitle());
        $reservablePage->setStructureType("comsa_reservable");
        $reservablePage->setParent($parent);
        $reservablePage->setLocale($locale);

        $url = $this->resourceLocatorStrategyPool
                ->getStrategyByWebspaceKey($webspaceKey)
                ->generate($reservable->getTitle(), $parent->getUuid(), $webspaceKey, $locale);

        $reservablePage->setResourceSegment($url);

        $reservablePage->getStructure()->bind([
           "title" => $reservable->getTitle(),
           "url" => $url,
           "product" => $reservable->getId()
        ]);

        $this->documentManager->persist($reservablePage, $locale);
        $this->documentManager->publish($reservablePage, $locale);
        $this->documentManager->flush();

        $this->reservableService->updatePage($reservable, $reservablePage->getUuid());
    }

    public function delete(Reservable $reservable) {
        try {
            $this->documentManager->remove(
                $this->documentManager->find($reservable->getPageId())
            );
            $this->documentManager->flush();
        } catch (\Exception $exception) {
            dump($exception->getMessage());
            die();
        }
    }
}
