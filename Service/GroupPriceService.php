<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Service;

use Comsa\SuluReservations\Entity\GroupPrice;
use Comsa\SuluReservations\Entity\Interfaces\CrudResource;
use Comsa\SuluReservations\Entity\Reservable;
use Comsa\SuluReservations\Entity\ReservableGroupPrice;
use Comsa\SuluReservations\Factory\GroupPriceFactory;
use Comsa\SuluReservations\Factory\ReservableGroupPriceFactory;
use Comsa\SuluReservations\Repository\GroupPriceRepository;
use Comsa\SuluReservations\Repository\ReservableGroupPriceRepository;
use Comsa\SuluReservations\Service\Interfaces\BaseCrudServiceInterface;
use Comsa\SuluReservations\Utility\TypeConverter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * CRUD Service for the GroupPrice class.
 * @package Comsa\SuluReservations\Service
 */
class GroupPriceService extends AbstractCrudService implements BaseCrudServiceInterface {
    private ReservableGroupPriceRepository $reservableGroupPriceRepository;

    public function __construct(EntityManagerInterface $entityManager, GroupPriceRepository $repository, ReservableGroupPriceRepository $reservableGroupPriceFactory) {
        $this->reservableGroupPriceRepository = $reservableGroupPriceFactory;

        parent::__construct($entityManager, $repository, GroupPrice::class);
    }

    public function create(array $data): CrudResource {
        $entity = GroupPriceFactory::create(
          $data["groupName"],
          $data["groupPrice"]
        );

        $this->save($entity);
        return $entity;
    }

    public function update(CrudResource $entity, array $data): void {
        // Won't update, only add new ones
    }

    public function map(?array $data): Collection {
        $pricePosition = 0;
        $groupPrices = new ArrayCollection();

        if (is_array($data)) {
            foreach ($data as $price) {
                $priceObject = $this->getRepository()->findOneByNameAndPrice(
                  $price["groupName"],
                  TypeConverter::stringToFloat((string) $price["groupPrice"])
                );

                if (!$priceObject) {
                    $priceObject = $this->create([
                        "groupName" => $price["groupName"],
                        "groupPrice" => TypeConverter::stringToFloat((string) $price["groupPrice"])
                    ]);
                }

                $groupPrices->add(
                    ReservableGroupPriceFactory::create(
                        null,
                        $priceObject,
                        $pricePosition
                    )
                );

                $pricePosition++;
            }
        }

        return $groupPrices;
    }

    public function clean(Reservable $reservable, ?array $data): Collection {
        $existingGroupPrices = [];
        $newGroupPrices = new ArrayCollection();
        $newReservableGroupPrices = new ArrayCollection();

        foreach ($reservable->getGroupPrices() as $existingGroupPrice) {
            $existingGroupPrices[$existingGroupPrice->getId()] = $existingGroupPrice->__toString();
        }

        if (is_array($data)) {
            $pricePosition = 0;
            foreach ($data as $groupPrice) {
                if (!in_array(
                    sprintf("%s%d", $groupPrice["groupName"], $groupPrice["groupPrice"]),
                    $existingGroupPrices
                )) {
                    $priceEntity = $this->getRepository()->findOneByNameAndPrice(
                      $groupPrice["groupName"],
                      TypeConverter::stringToFloat((string) $groupPrice["groupPrice"])
                    );

                    if(!$priceEntity) {
                        $priceEntity = $this->create([
                           "groupName" => $groupPrice["groupName"] ,
                           "groupPrice" => TypeConverter::stringToFloat((string) $groupPrice["groupPrice"])
                        ]);
                    }

                    $reservableGroupPrice = ReservableGroupPriceFactory::create(
                      $reservable,
                      $priceEntity,
                      $pricePosition
                    );

                    $this->entityManager->persist($reservableGroupPrice);
                    $newReservableGroupPrices->add($reservableGroupPrice);
                } else {
                    $id = array_search(
                        sprintf("%s%d", $groupPrice["groupName"], $groupPrice["groupPrice"]),
                        $existingGroupPrices
                    );

                    /** @var ReservableGroupPrice $priceToUpdate */
                    $priceToUpdate = $this->reservableGroupPriceRepository->find($id);
                    $priceToUpdate->setPosition($pricePosition);
                    $newReservableGroupPrices->add($priceToUpdate);
                }

                $newGroupPrices->add(sprintf(
                   "%s%d", $groupPrice["groupName"], $groupPrice["groupPrice"]
                ));

                $pricePosition++;
            }
        }

        foreach ($existingGroupPrices as $key => $existingGroupPrice) {
            if (!$newGroupPrices->contains($existingGroupPrice)) {
                $priceObject = $this->reservableGroupPriceRepository->find($key);
                $reservable->removeGroupPrice($priceObject);
                $this->entityManager->remove($priceObject);
            }
        }

        return $newReservableGroupPrices;
    }
}
