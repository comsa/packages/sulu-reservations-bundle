<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Service;

use Comsa\SuluReservations\Entity\Interfaces\CrudResource;
use Comsa\SuluReservations\Service\Interfaces\BaseCrudServiceInterface;
use Comsa\SuluReservations\Service\Interfaces\BaseOrmManagementInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Comsa\SuluReservations\Exception\InvalidCrudResourceException;

/**
 * Defines base save, delete, get repository and class validation.
 * Other services should extend this class and inject their own Service-specific repository and the name of the class for which the service was written.
 * @package Comsa\SuluReservations\Service
 */
abstract class AbstractCrudService implements BaseOrmManagementInterface {
    protected EntityManagerInterface $entityManager;
    private ServiceEntityRepository $repository;
    private string $expectedClass;

    public function __construct(EntityManagerInterface $entityManager, ServiceEntityRepository $repository, string $expectedClass) {
        $this->entityManager = $entityManager;
        $this->repository = $repository;
        $this->expectedClass = $expectedClass;
    }

    public function save(CrudResource $entity): void {
        $this->validateEntity($entity);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }

    public function delete(CrudResource $entity): void {
        $this->validateEntity($entity);
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }

    public function validateEntity(CrudResource $entity): void {
        if (!is_a($entity, $this->expectedClass)) {
            throw new InvalidCrudResourceException($this->expectedClass, $entity::class);
        }
    }

    public function getRepository(): ServiceEntityRepository {
        return $this->repository;
    }
}
