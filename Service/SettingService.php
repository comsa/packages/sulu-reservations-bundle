<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Service;

use Comsa\SuluReservations\Entity\Setting;
use Comsa\SuluReservations\Repository\SettingRepository;
use Comsa\SuluReservations\Service\Interfaces\BaseCrudServiceInterface;
use Comsa\SuluReservations\Entity\Interfaces\CrudResource;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * CRUD Service for the Setting Class.
 * @package Comsa\SuluReservations\Service
 */
class SettingService extends AbstractCrudService implements BaseCrudServiceInterface {
    public function __construct(EntityManagerInterface $entityManager, SettingRepository $repository) {
        parent::__construct($entityManager, $repository, Setting::class);
    }

    public function create(array $data): CrudResource {
        //-- Can't be created manually
    }

    /**
     * @param Setting $entity
     * @param array $data
     */
    public function update(CrudResource $entity, array $data): void {
        $this->validateEntity($entity);
        $entity->setValue($data["value"]);
        $this->save($entity);
    }
}
