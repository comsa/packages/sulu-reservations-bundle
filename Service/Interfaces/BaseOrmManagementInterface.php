<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Service\Interfaces;

use Comsa\SuluReservations\Entity\Interfaces\CrudResource;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;

interface BaseOrmManagementInterface {
    public function getRepository(): ServiceEntityRepository;
    public function save(CrudResource $entity): void;
    public function delete(CrudResource $entity): void;
    public function validateEntity(CrudResource $entity): void;
}
