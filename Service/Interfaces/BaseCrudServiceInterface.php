<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Service\Interfaces;

use Comsa\SuluReservations\Entity\Interfaces\CrudResource;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;

interface BaseCrudServiceInterface {
    public function create(array $data): CrudResource;
    public function update(CrudResource $entity, array $data): void;
}
