<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Service;

use Comsa\SuluReservations\Entity\Group;
use Comsa\SuluReservations\Entity\GroupPrice;
use Comsa\SuluReservations\Entity\Interfaces\CrudResource;
use Comsa\SuluReservations\Entity\Option;
use Comsa\SuluReservations\Entity\Reservable;
use Comsa\SuluReservations\Entity\Reservation;
use Comsa\SuluReservations\Enum\ReservableEnum;
use Comsa\SuluReservations\Factory\ReservationFactory;
use Comsa\SuluReservations\Repository\ReservationRepository;
use Comsa\SuluReservations\Service\Interfaces\BaseCrudServiceInterface;
use Comsa\SuluReservations\Utility\TypeConverter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * CRUD Service for the Reservation Class.
 * @package Comsa\SuluReservations\Service
 */
class ReservationService extends AbstractCrudService implements BaseCrudServiceInterface {
    private GroupService $groupService;
    private GroupPriceService $groupPriceService;
    private OptionService $optionService;

    public function __construct(
        EntityManagerInterface $entityManager,
        ReservationRepository $repository,
        GroupPriceService $groupPriceService,
        GroupService $groupService,
        OptionService $optionService
    ) {
        $this->groupPriceService = $groupPriceService;
        $this->groupService = $groupService;
        $this->optionService = $optionService;

        parent::__construct($entityManager, $repository, Reservation::class);
    }

    public function create(array $data): CrudResource {
        $reservation = ReservationFactory::create(
            $data["reservable"],
            $data["amount"],
            $data["suluDynamics"],
        );

        $reservation->setPrice(
            $this->calculatePrice(
                $data["reservable"],
                $reservation,
                $data["groupPrices"],
                $data["options"]
            )
        );

        $this->save($reservation);

        return $reservation;
    }

    public function updatePrice(Reservation $reservation, float $price): void {
        $reservation->setPrice($price);
        $this->save($reservation);
    }

    public function update(CrudResource $entity, array $data): void
    {
        // TODO: Implement update() method.
    }


    public function setPaid(Reservation $reservation) {
        $reservation->getPayment()->setPaid();
        $this->entityManager->flush();
    }

    public function updatePayment(Reservation $reservation, array $data): void {
        $reservation->setPrice($data["price"]);
        $reservation->setPayment($data["payment"]);

        $this->save($reservation);
    }

    public function calculatePrice(Reservable $reservable, Reservation $reservation, ?array $groups, ?array $options): float {
        $total = 0;

        if ($reservable->getPriceType() === ReservableEnum::PRICE_TYPE_SINGLE) {
            $total += $reservable->getSinglePrice();
            if ($groups !== null) {
                foreach ($groups as $id => $amount) {
                    $groupPrice = $this->groupPriceService->getRepository()->find($id);

                   $group = $this->groupService->create([
                        "reservation" => $reservation,
                        "group" => $groupPrice,
                        "amount" => $amount,
                        "price" => 0,
                    ]);
                }
            }
        } else {
            foreach ($groups as $id => $amount) {
                /** @var GroupPrice $groupPrice */
                $groupPrice = $this->groupPriceService->getRepository()->find($id);

                /** @var Group $group */
                $group = $this->groupService->create([
                    "reservation" => $reservation,
                    "group" => $groupPrice,
                    "amount" => TypeConverter::stringToInt((string) $amount),
                    "price" => $groupPrice->getPrice() * $amount
                ]);

                $total += $group->getPrice();
            }
        }

        $optionObjects = new ArrayCollection();

        foreach ($options as $id) {
            /** @var Option $option */
            $option = $this->optionService->getRepository()->find($id);
            $optionObjects->add($option);

            if ($reservable->getPriceType() === ReservableEnum::PRICE_TYPE_SINGLE) {
                $total += $option->getPrice();
            } else {
                $total += $option->getPrice() * $reservation->getAmount();
            }
        }

        $reservation->setOptions($optionObjects);

        return (float) $total;
    }
}
