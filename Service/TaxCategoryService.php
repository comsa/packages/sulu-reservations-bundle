<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Service;

use Comsa\SuluReservations\Entity\Interfaces\CrudResource;
use Comsa\SuluReservations\Entity\TaxCategory;
use Comsa\SuluReservations\Factory\TaxCategoryFactory;
use Comsa\SuluReservations\Repository\TaxCategoryRepository;
use Comsa\SuluReservations\Service\Interfaces\BaseCrudServiceInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * CRUD Service for the TaxCategory Class.
 * @package Comsa\SuluReservations\Service
 */
class TaxCategoryService extends AbstractCrudService implements BaseCrudServiceInterface {
    public function __construct(EntityManagerInterface $entityManager, TaxCategoryRepository $repository) {
        parent::__construct($entityManager, $repository, TaxCategory::class);
    }

    /**
     * @param array $data
     * @return CrudResource
     */
    public function create(array $data): CrudResource {
        $entity = TaxCategoryFactory::create(
            title: $data["title"],
            percentage: $data["percentage"]
        );

        $this->save($entity);

        return $entity;
    }

    /**
     * @param TaxCategory $entity
     * @param array $data
     * @throws \Comsa\SuluReservations\Exception\InvalidCrudResourceException
     */
    public function update(CrudResource $entity, array $data): void {
        $this->validateEntity($entity);
        $entity->setTitle($data["title"]);
        $entity->setPercentage($data["percentage"]);
        $this->save($entity);
    }

}
