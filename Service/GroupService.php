<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Service;

use Comsa\SuluReservations\Entity\Group;
use Comsa\SuluReservations\Entity\Interfaces\CrudResource;
use Comsa\SuluReservations\Factory\GroupFactory;
use Comsa\SuluReservations\Repository\GroupRepository;
use Comsa\SuluReservations\Service\Interfaces\BaseCrudServiceInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class GroupService extends AbstractCrudService implements BaseCrudServiceInterface {

    public function __construct(EntityManagerInterface $entityManager, GroupRepository $repository,) {
        parent::__construct($entityManager, $repository, Group::class);
    }

    public function save(CrudResource $entity): void {
        $this->entityManager->persist($entity);
    }

    public function create(array $data): CrudResource {
        $group = GroupFactory::create(
            $data["reservation"],
            $data["group"],
            $data["amount"],
            $data["price"]
        );

        $this->save($group);

        return $group;
    }

    public function update(CrudResource $entity, array $data): void
    {
        // TODO: Implement update() method.
    }

}
