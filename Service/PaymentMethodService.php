<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Service;

use Comsa\SuluReservations\Entity\Payment;
use Comsa\SuluReservations\Entity\PaymentMethod;
use Comsa\SuluReservations\Factory\PaymentMethodFactory;
use Comsa\SuluReservations\Repository\PaymentMethodRepository;
use Comsa\SuluReservations\Service\Interfaces\BaseCrudServiceInterface;
use Comsa\SuluReservations\Entity\Interfaces\CrudResource;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * CRUD Service for the PaymentMethod class.
 * @package Comsa\SuluReservations\Service
 */
class PaymentMethodService extends AbstractCrudService implements BaseCrudServiceInterface {
    public function __construct(EntityManagerInterface $entityManager, PaymentMethodRepository $repository) {
        parent::__construct($entityManager, $repository, PaymentMethod::class);
    }

    /**
     * @param PaymentMethod $entity
     */
    public function delete(CrudResource $entity): void {
        $this->validateEntity($entity);
        $entity->hide();
        $this->save($entity);
    }


    public function create(array $data): CrudResource {
        $entity =  PaymentMethodFactory::create(
            name: $data["name"],
            type: $data["type"],
            price: $data["price"]
        );

        $this->save($entity);

        return $entity;
    }

    /**
     * @param PaymentMethod $entity
     * @param array $data
     * @throws \Comsa\SuluShoppingCart\Exception\InvalidCrudResourceException
     */
    public function update(CrudResource $entity, array $data): void {
        $this->validateEntity($entity);
        $entity->setName($data["name"]);
        $entity->setPrice($data["price"]);
        $entity->show();

        $this->save($entity);
    }

}
