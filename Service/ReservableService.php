<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Service;

use Comsa\SuluReservations\Entity\Interfaces\CrudResource;
use Comsa\SuluReservations\Entity\Reservable;
use Comsa\SuluReservations\Entity\TaxCategory;
use Comsa\SuluReservations\Enum\ReservableEnum;
use Comsa\SuluReservations\Factory\ReservableFactory;
use Comsa\SuluReservations\Repository\ReservableRepository;
use Comsa\SuluReservations\Service\Interfaces\BaseCrudServiceInterface;
use Comsa\SuluReservations\Utility\TypeConverter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sulu\Bundle\FormBundle\Entity\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * CRUD Service for the Reservable Class.
 * @package Comsa\SuluReservations\Service
 */
class ReservableService extends AbstractCrudService implements BaseCrudServiceInterface {
    private GroupPriceService $groupPriceService;
    private OptionService $optionService;
    private TaxCategoryService $taxCategoryService;
    private ReservationService $reservationService;

    public function __construct(EntityManagerInterface $entityManager, ReservableRepository $repository, GroupPriceService $groupPriceService, OptionService $optionService, TaxCategoryService $taxCategoryService, ReservationService $reservationService) {
        $this->groupPriceService = $groupPriceService;
        $this->optionService = $optionService;
        $this->taxCategoryService = $taxCategoryService;
        $this->reservationService = $reservationService;

        parent::__construct($entityManager, $repository, Reservable::class);
    }

    /**
     * @param Reservable $entity
     */
    public function delete(CrudResource $entity): void {
        $entity->hide();
        $this->save($entity);
    }

    /**
     * @param array $data
     * @return CrudResource
     * @throws \Exception
     */
    public function create(array $data): CrudResource {
        $entity = ReservableFactory::create(
            $data["title"],
            $data["type"],
            $data["priceType"],
            $data["priceType"] === ReservableEnum::PRICE_TYPE_SINGLE ? TypeConverter::stringToFloat((string) $data["singlePrice"]) : null,
            $data["singleReservation"],
            $data["limit"],
            new \DateTime($data["start"]),
            new \DateTime($data["end"]),
            $this->getForm($data),
            $this->groupPriceService->map($data["groupPrices"]),
            $this->optionService->map($data["reservableOptions"]),
            $data["parentPage"],
            $data["optionSelectionType"],
            $this->getTaxCategory($data["taxCategory"])
        );

        $this->save($entity);
        return $entity;
    }

    /**
     * @param Reservable $entity
     * @param array $data
     */
    public function update(CrudResource $entity, array $data): void {
        $entity->setTitle($data["title"]);
        $entity->setType($data["type"]);
        $entity->setPriceType($data["priceType"]);
        $entity->setSinglePrice(
            $data["priceType"] === ReservableEnum::PRICE_TYPE_SINGLE ?
                TypeConverter::stringToFloat((string) $data["singlePrice"]) :
                null
        );
        $entity->setSingleReservation($data["singleReservation"]);
        $entity->setLimit($data["limit"]);
        $entity->setStart(new \DateTime($data["start"]));
        $entity->setEnd(new \DateTime($data["end"]));
        $entity->setGroupPrices(
            $this->groupPriceService->clean($entity, $data["groupPrices"])
        );
        $entity->setOptions(
            $this->optionService->clean($entity, $data["reservableOptions"])
        );
        $entity->setForm($this->getForm($data));
        $entity->setParentPage($data["parentPage"]);
        $entity->setOptionSelectionType($data["optionSelectionType"]);
        $entity->setTaxCategory($this->getTaxCategory($data["taxCategory"]));
        $entity->show();

        $this->save($entity);
    }

    public function updatePage(Reservable $entity, string $pageId): void {
        $entity->setPageId($pageId);
        $this->save($entity);
    }

    private function getForm(array $data): Form {
        if (is_array($data["form"])) {
            return $this->entityManager->getRepository(Form::class)->find($data["form"]["id"]);
        }

        return $this->entityManager->getRepository(Form::class)->find($data["form"]);
    }

    private function getTaxCategory(null|int|array $id): ?TaxCategory {
        if (is_array($id)) {
            $id = $id["id"];
        }

        if (!$id) {
            return null;
        }

        return $this->taxCategoryService->getRepository()->find($id);
    }

    public function calculateSpacesLeft(Reservable $reservable) {
        $reservations = $this->reservationService->getRepository()->findByReservable($reservable);

        return $reservable->getLimit() - count($reservations);
    }
}
