<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\EventSubscriber;

use App\Entity\CartItem;
use Comsa\SuluReservations\Entity\Setting;
use Comsa\SuluReservations\Event\ReservationConfirmedEvent;
use Comsa\SuluReservations\Factory\EmailFactory;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

/**
 * Listener that handles the mailing of completed Reservations.
 * @package Comsa\SuluReservations\EventSubscriber
 */
class ReservationSubscriber implements EventSubscriberInterface
{
    private MailerInterface $mailer;
    private ParameterBagInterface $parameterBag;
    private TranslatorInterface $translator;
    private Environment $twig;
    private EntityManagerInterface $em;
    private EntityRepository $settingRepository;

    public function __construct(MailerInterface $mailer, ParameterBagInterface $parameterBag, TranslatorInterface $translator, Environment $twig, EntityManagerInterface $em)
    {
        $this->mailer = $mailer;
        $this->parameterBag = $parameterBag;
        $this->translator = $translator;
        $this->twig = $twig;
        $this->em = $em;
        $this->settingRepository = $this->em->getRepository(Setting::class);
    }

    public static function getSubscribedEvents()
    {
        return [
            ReservationConfirmedEvent::NAME => "onReservationConfirmed",
        ];
    }

    public function onReservationConfirmed(ReservationConfirmedEvent $event)
    {
        $reservation = $event->getReservation();
        $fromEmail = trim($reservation->getFormdata()->getForm()->serializeForLocale("nl")["fromEmail"]);
        $toEmail = trim($reservation->getFormdata()->getForm()->serializeForLocale("nl")["toEmail"]);
        $customerEmail = trim($reservation->getFormdata()->getData()["email"]);

        $email = EmailFactory::create(
            from: $fromEmail,
            to: [ $customerEmail ],
            cc: [ $fromEmail ],
            bcc: "notification@comsa.be",
            subject: $this->translator->trans("sulu_reservations.confirmation_subject"),
            template: "@SuluReservations/mails/build/reservation-confirmation.html.twig",
            params: [
                "reservation" => $reservation
            ]
        );

        $this->mailer->send($email);
    }
}

