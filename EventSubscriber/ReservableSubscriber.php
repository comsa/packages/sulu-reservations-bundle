<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\EventSubscriber;

use Comsa\SuluReservations\Event\Reservable\ReservableCreatedEvent;
use Comsa\SuluReservations\Event\Reservable\ReservableDeletedEvent;
use Comsa\SuluReservations\Event\Reservable\ReservableUpdatedEvent;
use Comsa\SuluReservations\Service\DocumentService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscriber that handles all Reservable events
 * @package Comsa\SuluReservations\EventSubscriber
 */
class ReservableSubscriber implements EventSubscriberInterface {
    private DocumentService $documentService;

    public function __construct(DocumentService $documentService) {
        $this->documentService = $documentService;
    }

    public static function getSubscribedEvents() {
        return [
            ReservableCreatedEvent::NAME => ["onReservableCreated", 10],
            ReservableUpdatedEvent::NAME => ["onReservableUpdated", 10],
            ReservableDeletedEvent::NAME => ["onReservableDeleted", 10]
        ];
    }

    public function onReservableCreated(ReservableCreatedEvent $event): void {
        $this->documentService->create(
            $event->getReservable(),
            $event->getLocale(),
            $event->getWebspaceKey()
        );
    }

    public function onReservableUpdated(ReservableUpdatedEvent $event): void {
        $this->documentService->create(
            $event->getReservable(),
            $event->getLocale(),
            $event->getWebspaceKey()
        );
    }

    public function onReservableDeleted(ReservableDeletedEvent $event) {
        $this->documentService->delete($event->getReservable());
    }
}
