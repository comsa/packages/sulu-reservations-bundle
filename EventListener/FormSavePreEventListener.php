<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\EventListener;

use Comsa\SuluReservations\Entity\Reservable;
use Comsa\SuluReservations\Manager\ReservationManager;
use Comsa\SuluReservations\Service\ReservableService;
use Comsa\SuluReservations\Service\ReservationService;
use Comsa\SuluReservations\Utility\TypeConverter;
use Doctrine\ORM\EntityManagerInterface;
use Sulu\Bundle\FormBundle\Entity\Dynamic;
use Sulu\Bundle\FormBundle\Entity\Form;
use Sulu\Bundle\FormBundle\Event\FormSavePreEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

/**
 * Stores the form as a Sulu Form
 * @package Comsa\SuluReservations\EventListener
 */
class FormSavePreEventListener implements EventSubscriberInterface
{
    private EntityManagerInterface $entityManager;
    private RouterInterface $router;
    private RequestStack $requestStack;
    private ReservableService $reservableService;
    private ReservationService $reservationService;
    private ReservationManager $reservationManager;

    public function __construct(
        EntityManagerInterface $entityManager,
        RouterInterface $router,
        RequestStack $requestStack,
        ReservableService $reservableService,
        ReservationService $reservationService,
        ReservationManager $reservationManager
    ) {
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->requestStack = $requestStack;
        $this->reservableService = $reservableService;
        $this->reservationService = $reservationService;
        $this->reservationManager = $reservationManager;
    }

    public static function getSubscribedEvents()
    {
        return [
          FormSavePreEvent::NAME => "onSubmit"
        ];
    }

    public function onSubmit(FormSavePreEvent $event) {
        $request = $this->requestStack->getCurrentRequest();

        if (!$request->request->get("reservable")) {
            return;
        }

        $baseDynamicFormName = "dynamic_form";

        $uuid = $request->request->get("uuid");
        $webspace = $request->request->get("webspace");
        $locale = $request->request->get("locale");
        $groupPrices = $request->request->get("groupPrices");
        $options = $request->request->get("options");

        /** @var Form $suluForm */
        $suluForm = $this->entityManager->getRepository(Form::class)->find($request->request->get("form"));
        $dynamicFormName = $baseDynamicFormName . $suluForm->getId();
        $dynamicFormData = $request->request->get($dynamicFormName);

        $suluDynamics = new Dynamic(
            "page",
            $uuid,
            $locale,
            $suluForm,
            $dynamicFormData,
            $webspace,
            $suluForm->getTranslation($locale)->getTitle()
        );

        $this->entityManager->persist($suluDynamics);

        /** @var Reservable $reservable */
        $reservable = $this->reservableService->getRepository()->find($request->request->get("reservable"));

        $reservation = $this->reservationService->create([
            "reservable" => $reservable,
            "amount" => TypeConverter::stringToInt((string) $request->request->get("total")),
            "suluDynamics" => $suluDynamics,
            "groupPrices" => $groupPrices,
            "options" => $options
        ]);

        $this->reservationManager->set($reservation);

        return new RedirectResponse($this->router->generate("comsa_sulu_reservations_select_payment_method"));
    }
}
