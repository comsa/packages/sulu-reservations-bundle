<?php

declare(strict_types=1);

namespace Comsa\SuluReservations\Manager;

use Comsa\SuluReservations\Entity\Reservation;
use Comsa\SuluReservations\Service\ReservationService;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Manager for Reservations
 * @package Comsa\SuluReservations\Manager
 */
class ReservationManager
{
    private SessionInterface $session;
    private ReservationService $reservationService;

    public function __construct(SessionInterface $session, ReservationService $reservationService) {
        $this->session = $session;
        $this->reservationService = $reservationService;
    }

    public function set(Reservation $reservation) {
        $this->session->set("reservation", $reservation);
    }

    public function get(bool $forceNew = false): Reservation {
        if ($this->session->has("reservation") &&
            $this->session->get("reservation") instanceof Reservation &&
            !$forceNew
        ) {
            return $this->reservationService->getRepository()->find($this->session->get("reservation")->getId());
        }
    }
}
